<!DOCTYPE html>
<html class="no-js" lang="en">
  <head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Giya | Maging a Guide</title>
    <link rel="stylesheet" type="text/css" href="/assets/css/normalize.css">
  </head>
  <body>
    <?php
    // Turn off all error reporting
    error_reporting(0);
    $host = "http://52.10.152.124:8000";
    function httpPost($url,$params) {
    $ch = curl_init();  
      curl_setopt($ch,CURLOPT_URL,$url);
      curl_setopt($ch,CURLOPT_RETURNTRANSFER,true);
      curl_setopt($ch,CURLOPT_HEADER, false); 
      curl_setopt($ch, CURLOPT_POSTFIELDS, $params);    
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);                                                                      
      curl_setopt($ch, CURLOPT_HTTPHEADER, array(                                                                          
        'Content-Type: application/json',                                                                                
        'Content-Length: ' . strlen($params)));
      $output=curl_exec($ch);
      curl_close($ch);
      return $output;
  } ?>
    <table cellpadding="0" border="0" width="100%">
        <tr width="100%">
          <td align="center" bgcolor="#d4e034" width="90%">
            <p><center><a href="index.php"><img src="/assets/img/giya-logo.gif"></a></center></p>
          </td>
          <td align="right" bgcolor="#4d4d4d" width="10%">
            <p><center><a href="login.php"><img src="/assets/img/login.png"></a></center></p>
          </td>
        </tr>
        <tr>
          <td align="center" bgcolor="#f5faf3" colspan="2">
            <font color="#4d4d4d"><p>Kung nais mong mailista ang iyong pangalan at impormasyon bilang isang gabay/tour guide sa website na ito, punan ang mga detalyeng hinihingi sa baba.</p></font>
            <?php
            // Turn off all error reporting
            error_reporting(0);
            if(isset($_POST["name"]) && isset($_POST["contact_info"]) && isset($_POST["street"]) && isset($_POST["city"]) && isset($_POST["province"]) && isset($_POST["service"])) {
              if("" != $_POST["name"] && "" != $_POST["contact_info"] && "" != $_POST["street"] && "" != $_POST["city"] && "" != $_POST["province"] && "" != $_POST["service"]) {
                // submit form
                $jsonBuilder = array('guide_name' => $_POST["name"], 'contact_info' => $_POST["contact_info"], 'street' => $_POST["street"], 'city' => $_POST["city"], 'province' => $_POST["province"], 'service' => $_POST["service"]);
                $data = json_encode($jsonBuilder);

                $options = array(
                  'http' => array(
                  'header'  => "Content-type: application/json",
                    'method'  => 'POST',
                    'content' => $data,
                  ),
                );
                $result = httpPost($host . "/v1/application",$data);
                if($result["success"] == true) {
                  $hasSubmitted = true; ?>
                  <h2> Application sent successfully. </h2>
                <?php } else {
                  $hasSubmitted = false; ?>
                  <h2> Application failed to send, please try again. </h2>
                <?php }
              } else { 
                $hasSubmitted = false; ?>
                <h2> Please fill up all the fields in the form </h2>
              <?php }
            } ?>
            <form action="form.php" method="POST">
              <?php if($hasSubmitted == false && isset($_POST["name"])) { ?>
                <p><input type="text" class="textbox" name="name" style="padding: 2px 10px 5px 5px; width:60%;" value="<?php echo $_POST['name'] ?>"></p>
              <?php } else { ?>
                <p><input type="text" class="textbox" name="name" style="padding: 2px 10px 5px 5px; width:60%;" value="Name"></p>
              <?php } ?>
              <?php if($hasSubmitted == false && isset($_POST["contact_info"])) { ?>
                <p><input type="text" class="textbox" name="contact_info" style="padding: 2px 10px 5px 5px; width:60%;" value="<?php echo $_POST['contact_info'] ?>"></p>
              <?php } else { ?>
                <p><input type="text" class="textbox" name="contact_info" style="padding: 2px 10px 5px 5px; width:60%;" value="Contact Info"></p>
              <?php } ?>
              <?php if($hasSubmitted == false && isset($_POST["street"])) { ?>
                <p><input type="text" class="textbox" name="street" style="padding: 2px 10px 5px 5px; width:60%;" value="<?php echo $_POST['street'] ?>"></p>
              <?php } else { ?>
                <p><input type="text" class="textbox" name="street" style="padding: 2px 10px 5px 5px; width:60%;" value="Street"></p>
              <?php } ?>
              <?php if($hasSubmitted == false && isset($_POST["city"])) { ?>
                <p><input type="text" class="textbox" name="city" style="padding: 2px 10px 5px 5px; width:60%;" value="<?php echo $_POST['city'] ?>"></p>
              <?php } else { ?>
                <p><input type="text" class="textbox" name="city" style="padding: 2px 10px 5px 5px; width:60%;" value="City"></p>
              <?php } ?>
              <?php if($hasSubmitted == false && isset($_POST["province"])) { ?>
                <p><input type="text" class="textbox" name="province" style="padding: 2px 10px 5px 5px; width:60%;" value="<?php echo $_POST['province'] ?>"></p>
              <?php } else { ?>
                <p><input type="text" class="textbox" name="province" style="padding: 2px 10px 5px 5px; width:60%;" value="Province"></p>
              <?php } ?>
              <?php if($hasSubmitted == false && isset($_POST["service"])) { ?>
                <p><textarea name="service" class="textbox" style="padding: 2px 10px 5px 5px; width:60%;"><?php echo $_POST['service'] ?></textarea></p>
              <?php } else { ?>
                <p><textarea name="service" class="textbox" style="padding: 2px 10px 5px 5px; width:60%;">Service</textarea>
              <?php } ?>
              <p><input type="submit" size="10" style="background-color:#b94826; border:none; color:#ffffff; font-size:14px; padding:8px 30px 8px 30px;" name="SUBMIT" value="SUBMIT" class="submit-button"></p>
            </form>
          </td>
        </tr>
        <tr>
          <td align="center" bgcolor="#D4E034" colspan="2">
            <p>
              <font color="#4d4d4d">
                &copy; 2015 Alpas
                <br/>
                <a href="index.php">Home</a> | <a href="about.php">About</a> | <a href="contact-us.php">Contact Us</a> | <a href="http://giya.voyager.ph">Full Version</a>
              </font>
            </p>
          </td>
        </tr>
    </table>
  </body>
</head>
</html>