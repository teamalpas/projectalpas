<!DOCTYPE html>
<html class="no-js" lang="en">
  <head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Giya | Home</title>
      <link rel="stylesheet" type="text/css" href="/assets/css/normalize.css">
  </head>
  <body>
    <?php
    // Turn off all error reporting
    error_reporting(0);
    $host = "http://52.10.152.124:8000";
    function httpGet($url) {
      $ch = curl_init();
      curl_setopt($ch,CURLOPT_URL,$url);
      curl_setopt($ch,CURLOPT_RETURNTRANSFER,true);
      $output=curl_exec($ch);
      curl_close($ch);
      return $output;
    } ?>
    <table border="0" cellpadding="5" width="100%">
      <tr>
        <td align="center" bgcolor="#d4e034">
            <p><a href="index.php"><img src="/assets/img/giya-mainlogo.gif"></a></p>
            <p><font color="#8e8c35">
              Where travelers and<br/>local guides meet.
            </font></p>
            <form action="search-result.php" method="POST">
                <p><select name="provinces" id="provinces">
                  <?php if(!isset($_POST["provinces"]) || "All"==$_POST["provinces"]) { ?>
                    <option selected="selected" value="All">Where do you want to go?</option>
                  <?php }
                  $content = httpGet($host . "/v1/provinces");
                  $json = json_decode($content, true);

                  foreach($json['result'] as $item) {
                    if($item == $_POST["provinces"] || $item == $_GET["province"]) { ?>
                      <option selected="selected" value="<?php echo $item ?>"><?php echo $item ?></option>
                    <?php } else { ?>
                      <option value="<?php echo $item ?>"><?php echo $item ?></option>
                    <?php }
                  } ?>
                </select></p>
                <p><select name="activities" id="activities">
                  <?php if(!isset($_POST["activities"]) || "All"==$_POST["activities"]) { ?>
                    <option selected="selected" value="All">What do you want to do?</option>
                  <?php }
                  $content = httpGet($host . "/v1/activities");
                  $json = json_decode($content, true);
                  foreach($json['result'] as $item) {
                    if($item == $_POST["activities"] || $item == $_GET["activity"]) { ?>
                      <option selected="selected" value="<?php echo $item ?>"><?php echo $item ?></option>
                    <?php } else { ?>
                      <option value="<?php echo $item ?>"><?php echo $item ?></option>
                    <?php }
                  } ?>
                </select></p>
                <p><input type="submit" size="10" style="background-color:#b94826; border:none; color:#ffffff; font-size:14px; padding:8px 30px 8px 30px;" value="FIND A GUIDE"/></p>
            </form>
        </td>
      </tr>
      <tr>
        <td align="center">
            <p>
                <font color="#6d6c00" size="6">LOKAL KA BA?</font>
                <br/>
                <font color="#808080">Mag-apply upang maging bahagi ng aming listahan ng mga gabay (tour guide).
                </font>
            </p>
            <p>
                <form action="form.php">
                    <input type="submit" size="10" style="background-color:#b94826; border:none; color:#ffffff; font-size:14px; padding:8px 30px 8px 30px;" value="MAG-APPLY "/>
                </form>
                <br/>
                <form action="login.html">
                    <input type="submit" size="10" style="background-color:#b94826; border:none; color:#ffffff; font-size:14px; padding:8px 30px 8px 30px;" value="MAG-LOG IN"/>
                </form>
            </p>
        </td>
      </tr>
      <tr>
        <td align="center" bgcolor="#D4E034">
            <p>
              <font color="#4d4d4d">
                &copy; 2015 Alpas
                <br/>
                Home | <a href="about.php">About</a> | <a href="contact-us.php">Contact Us</a> | <a href="http://giya.voyager.ph">Full Version</a>
              </font>
            </p>
        </td>
      </tr>
    </table>
  </body>
</head>
</html>