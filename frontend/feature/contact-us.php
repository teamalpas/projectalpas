<!DOCTYPE html>
<html class="no-js" lang="en">
  <head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Giya | Contact Us</title>
    <link rel="stylesheet" type="text/css" href="/assets/css/normalize.css">
  </head>
  <body>
    <?php
    // Turn off all error reporting
    error_reporting(0);
    $host = "http://52.10.152.124:8000";
    function httpGet($url) {
      $ch = curl_init();  
      curl_setopt($ch,CURLOPT_URL,$url);
      curl_setopt($ch,CURLOPT_RETURNTRANSFER,true);
      $output=curl_exec($ch);
      curl_close($ch);
      return $output;
    } ?>
    <table border="0" cellpadding="5" width="100%">
        <tr width="100%">
          <td align="center" bgcolor="#d4e034" width="90%">
            <p><center><a href="index.php"><img src="/assets/img/giya-logo.gif"></a></center></p>
          </td>
          <td align="right" bgcolor="#4d4d4d" width="10%">
            <p><center><a href="login.php"><img src="/assets/img/login.png"></a></center></p>
          </td>
        </tr>
        <tr>
        <tr>
          <td align="center" bgcolor="#f5faf4" colspan="2">
            <font color="#6d6e2e">
              <h3>CONTACT US</h3>
            </font>
            <table border="0" cellpadding="7" width="50%">
              <tr>
                <td><img src="/assets/img/icon-phone.png"></td>
                <td><font color="#808080" size="5">+63 947 987 6543</font></td>
              </tr>
              <tr>
                <td><img src="/assets/img/icon-email.png"></td>
                <td><font color="#808080" size="5">giyaguidesph@gmail.com</font></td>
              </tr>
              <tr>
                <td><img src="/assets/img/icon-fb.png"></td>
                <td><font color="#808080" size="5">facebook.com/GiyaGuidesPH</font></td>
              </tr>
              <tr>
                <td><img src="/assets/img/icon-twitter.png"></td>
                <td><font color="#808080" size="5">@giyaguidesph</font></td>
              </tr>
            </table>
          </td>
        </tr>
        <tr>
          <td align="center" bgcolor="#D4E034" colspan="2">
            <p>
              <font color="#4d4d4d">
                &copy; 2015 Alpas
                <br/>
                <a href="index.php">Home</a> | <a href="about.php">About</a> | <a href="contact-us.php">Contact Us</a> | <a href="http://giya.voyager.ph">Full Version</a>
              </font>
            </p>
          </td>
        </tr>
    </table>
  </body>
</head>
</html>