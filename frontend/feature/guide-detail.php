<!DOCTYPE html>
<html class="no-js" lang="en">
   <head>
      <meta charset="utf-8" />
      <meta name="viewport" content="width=device-width, initial-scale=1.0" />
      <title>Giya | Search Results</title>
   </head>
   <body>
      <?php
         // Turn off all error reporting
         error_reporting(0);
         $host = "http://52.10.152.124:8000";
         function httpGet($url) {
           $ch = curl_init();  
           curl_setopt($ch,CURLOPT_URL,$url);
           curl_setopt($ch,CURLOPT_RETURNTRANSFER,true);
           $output=curl_exec($ch);
           curl_close($ch);
           return $output;
         } ?>
      <table border="0" cellpadding="5" width="100%">
         <tr>
            <td align="center" bgcolor="#d4e034">
               <p><a href="index.php"><img src="/assets/img/giya-logo.gif"></a></p>
               <form action="search-result.php" method="POST">
                  <p>
                     <select name="provinces" id="provinces">
                        <?php if(!isset($_POST["provinces"]) || "All"==$_POST["provinces"]) { ?>
                        <option selected="selected" style="width:60%;" value="All">Where do you want to go?</option>
                        <?php }
                           $content = httpGet($host . "/v1/provinces");
                           $json = json_decode($content, true);
                           
                           foreach($json['result'] as $item) {
                             if($item == $_POST["provinces"] || $item == $_GET["province"]) { ?>
                        <option selected="selected" style="width:60%;" value="<?php echo $item ?>"><?php echo $item ?></option>
                        <?php } else { ?>
                        <option style="width:60%;" value="<?php echo $item ?>"><?php echo $item ?></option>
                        <?php }
                           } ?>
                     </select>
                  </p>
                  <p>
                     <select name="activities" id="activities">
                        <?php if(!isset($_POST["activities"]) || "All"==$_POST["activities"]) { ?>
                        <option selected="selected" style="width:60%;" value="All">What do you want to do?</option>
                        <?php }
                           $content = httpGet($host . "/v1/activities");
                           $json = json_decode($content, true);
                           foreach($json['result'] as $item) {
                             if($item == $_POST["activities"] || $item == $_GET["activity"]) { ?>
                        <option selected="selected" style="width:60%;" value="<?php echo $item ?>"><?php echo $item ?></option>
                        <?php } else { ?>
                        <option style="width:60%;" value="<?php echo $item ?>"><?php echo $item ?></option>
                        <?php }
                           } ?>
                     </select>
                  </p>
                  <p><input type="submit" size="10" style="background-color:#b94826; border:none; color:#ffffff; font-size:14px; padding:8px 30px 8px 30px;" value="FIND A GUIDE"/></p>
               </form>
            </td>
         </tr>
         <tr>
            <td>
              <img align="middle" src="/assets/img/mapmarker.png"/>
              <?php if(isset($_POST["provinces"])) {
               echo $_POST["provinces"]; 
               echo '>';
               } else if(isset($_GET["province"])) { 
               echo $_GET["province"]; 
               echo '>'; 
               }
                 if(isset($_POST["activities"])) { 
                  echo $_POST["activities"]; 
                  } else if(isset($_GET["activity"])) { 
                  echo $_GET["activity"]; 
                  }
                 if(isset($_GET["destination"])) { 
                  echo '>'; 
                  echo $_GET["destination"]; 
               } ?>
            </td>
         </tr>
         <tr>
          <td align="center">
            <table border="1" bordercolor="#eeeeee" cellpadding="15" width="65%">
              <?php if(isset($_GET["guide"]) && isset($_GET["location"]) && isset($_GET["destination"]) && isset($_GET["province"]) && isset($_GET["activity"])) {
                  $province = $_GET["province"];
                  $activity = $_GET["activity"];
                  $destination = $_GET["destination"];
                  $location = $_GET["location"];
                  $guide = $_GET["guide"];
                  $api = '/v1/guides/' . $_GET["guide"];
                  $content = httpGet($host . $api);
                  $json = json_decode($content, true);
                  $entry = $json["results"];
                  if("true" == $json["success"]) { ?>
              <tr>
                <td>
                  <p align="center"><img src="/assets/img/default-photo.png"/></p>
                  <p>
                    <font color="#4d4d4d" size="4"><?php echo $entry["complete_name"]; ?></font>
                    <br/>
                    <img align="top" width="11" src="/assets/img/mapmarker.png"><font color="#818181" size="2"><?php echo $destination . "," . $province; ?></font>
                  </p>
                  <p>
                    <font color="#b74926" size="3"><b>Services included</b></font>
                    <br/>
                    <font color="#4d4d4d" size="3"><?php echo $entry["full_description"]; ?></font>
                  </p>
                  <br>
                  <p>
                    <font color="#4d4d4d" size="2">Guide Fee</font>
                    <br/>
                    <input disabled="disabled" style="background-color:#d3df33; border:0; color:#1a1a1a; font-size:14px; padding:9px 8px 9px 8px" type="submit" value="P <?php echo $entry["rate_details"]; ?>"/>
                    <br/>
                    <font color="#cbcbcb"><sub><i>*Price may vary depending on size of group</i></sub></font>
                  </p>
                  <p align="center">
                    <input style="background-color:#4d4d4d; border:0; color:#ffffff; font-size:14px; padding:9px 8px 9px 8px" type="submit" value="<?php echo $entry["contact_details"] ?>"/>
                  <br/>
                  <br/>
                  <?php if(!empty($entry["facebook"])) {; ?><a href="<?php echo $entry['facebook'] ?>">
                    <input style="background-color:#3b5999; border:0; color:#ffffff; font-size:14px; padding:9px 8px 9px 8px" type="submit" value="Contact via Facebook"/>
                  </a><?php } ?>
                  </p>
                </td>
              </tr>
            </table>
          </td>
         </tr>
         <tr>
            <td align="center" bgcolor="#D4E034">
               <p>
                <font color="#4d4d4d">
                  &copy; 2015 Alpas
                  <br/>
                  <a href="index.php">Home</a> | <a href="about.php">About</a> | <a href="contact-us.php">Contact Us</a> | <a href="http://giya.voyager.ph">Full Version</a>
                </font>
               </p>
            </td>
         </tr>
        <?php }
        } ?>
      </table>
   </body>
   </head>
</html>