<!DOCTYPE html>
<html class="no-js" lang="en">
   <head>
      <meta charset="utf-8" />
      <meta name="viewport" content="width=device-width, initial-scale=1.0" />
      <title>Giya | Search Results</title>
   </head>
   <body>
      <?php
         // Turn off all error reporting
         error_reporting(0);
         $host = "http://52.10.152.124:8000";
         function httpGet($url) {
           $ch = curl_init();  
           curl_setopt($ch,CURLOPT_URL,$url);
           curl_setopt($ch,CURLOPT_RETURNTRANSFER,true);
           $output=curl_exec($ch);
           curl_close($ch);
           return $output;
         } ?>
      <table border="0" bgcolor="#d4e034" cellpadding="5" width="100%">
        <tr width="100%">
          <td align="center" bgcolor="#d4e034" width="90%">
            <p><center><a href="index.php"><img src="/assets/img/giya-logo.gif"></a></center></p>
          </td>
          <td align="right" bgcolor="#4d4d4d" width="10%">
            <p><center><a href="login.php"><img src="/assets/img/login.png"></a></center></p>
          </td>
        </tr>
        <tr>
          <td align="center" bgcolor="#d4e034" colspan="2">
            <form action="search-result.php" method="POST">
              <p>
                <select name="provinces" id="provinces">
                    <?php if(!isset($_POST["provinces"]) || "All"==$_POST["provinces"]) { ?>
                    <option selected="selected" style="width:60%;" value="All">Where do you want to go?</option>
                    <?php }
                       $content = httpGet($host . "/v1/provinces");
                       $json = json_decode($content, true);
                       
                       foreach($json['result'] as $item) {
                         if($item == $_POST["provinces"] || $item == $_GET["province"]) { ?>
                    <option selected="selected" style="width:60%;" value="<?php echo $item ?>"><?php echo $item ?></option>
                    <?php } else { ?>
                    <option style="width:60%;" value="<?php echo $item ?>"><?php echo $item ?></option>
                    <?php }
                       } ?>
                 </select>
              </p>
              <p>
                 <select name="activities" id="activities">
                    <?php if(!isset($_POST["activities"]) || "All"==$_POST["activities"]) { ?>
                    <option selected="selected" style="width:60%;" value="All">What do you want to do?</option>
                    <?php }
                       $content = httpGet($host . "/v1/activities");
                       $json = json_decode($content, true);
                       foreach($json['result'] as $item) {
                         if($item == $_POST["activities"] || $item == $_GET["activity"]) { ?>
                    <option selected="selected" style="width:60%;" value="<?php echo $item ?>"><?php echo $item ?></option>
                    <?php } else { ?>
                    <option style="width:60%;" value="<?php echo $item ?>"><?php echo $item ?></option>
                    <?php }
                       } ?>
                 </select>
              </p>
              <p><input type="submit" size="10" style="background-color:#b94826; border:none; color:#ffffff; font-size:14px; padding:8px 30px 8px 30px;" value="FIND A GUIDE"/></p>
            </form>
          </td>
         </tr>
         <tr bgcolor="#eeeeee">
            <td colspan="2">
              <?php if(isset($_POST["provinces"])) { ?>
              <?php echo $_POST["provinces"]; ?>
              <?php echo '>'; ?> </li>
              <?php } else if(isset($_GET["province"])) { ?>
              <?php echo $_GET["province"]; ?>
              <?php echo '>'; ?>
              <?php }
                 if(isset($_POST["activities"])) { ?>
                 <?php echo $_POST["activities"]; ?>
                 <?php } else if(isset($_GET["activity"])) { ?>
                 <?php echo $_GET["activity"]; ?>
                 <?php }
                 if(isset($_GET["destination"])) { ?>
                 <?php echo '>'; ?>
                 <?php echo $_GET["destination"]; ?>
              <?php } ?>
               <?php if(isset($_GET["location"]) && isset($_GET["destination"]) && isset($_GET["province"]) && isset($_GET["activity"])) {
                  if(isset($_GET["province"]) && isset($_GET["activity"])) {
                    $province = $_GET["province"];
                    $activity = $_GET["activity"];
                    $destination = $_GET["destination"];
                    $location = $_GET["location"];
                  }
                  $api = '/v1/locations/' . $_GET["location"];
                  if(isset($_GET["page"])) {
                    $api = $api . '?page=' . $_GET["page"];
                  }
                  $content = httpGet($host . $api);
                  $json = json_decode($content, true);
                  if("true" == $json["success"]) { ?>
                  <center>
                    <table>
                      <tr bgcolor="#eeeeee">
                        <td>
                          <img src="/assets/img/view-photo.jpg"/>
                        </td>
                      </tr>
                      <tr>
                        <td>
                          <font color="#808080" size="3">Malapascua Island is a beautiful island in Cebu</font>
                        </td>
                      </tr>
                    </table>
                    <ul type="none">
                       <?php foreach ($json["results"] as $entry) { ?>
                       <li class="guide-list-entry" type="none">
                        <table align="center" bgcolor="#eeeeee" border="0" cellpadding="10px" cellspacing="10px" margin="10px" width="100%">
                            <tr nowrap>
                               <td colspan="2" padding="20">
                                <img src="/assets/img/default-photosmall.png"/>
                                <font color="#4d4d4d" size="5"> <?php echo $entry["complete_name"]; ?></font>
                                <hr/>
                               </td>
                            </tr>
                            <tr>
                              <td width="70%">
                                <font color="#4d4d4d" size="2"><?php echo $entry["service_description"]; ?></font>
                              </td>
                              <td width="25%">
                                <input disabled="disabled" style="background-color:#d3df33; border:0; color:#1a1a1a; font-size:14px; padding:9px 8px 9px 8px" type="submit" value="P <?php echo $entry["rate_details"]; ?>"/>
                              </td>
                            </tr>
                            <tr>
                              <td align="center" bgcolor="#999999" colspan="2" height="5">
                                <a href="guide-detail.php?province=<?php echo $province ?>&activity=<?php echo $activity ?>&destination=<?php echo $destination ?>&location=<?php echo $location ?>&guide=<?php echo $entry['guide_id'] ?>">
                                  <input style="background-color:#999999; border:0; color:#ffffff; font-size:14px; padding:9px 8px 9px 8px" type="submit" value="VIEW DETAILS"/>
                                </a>
                              </td>
                            </tr>
                         </table>
                       </li>
                       <?php } ?>
                    </ul>
                  </center>
               <br>
               <?php echo "Pages: ";
                  $currentPage = $json["current_page"];
                  $totalPages = $json["total_pages"];
                  if(is_numeric($currentPage) && is_numeric($totalPages)) {
                    for($i=1; $i<=$totalPages; $i++) { ?> 
               <?php if($i == $currentPage) {
                  echo $i;
                  } else { ?>
               <a href="search-result.php?province=<?php echo $province ?>&activity=<?php echo $activity ?>&destination=<?php echo $destination ?>&location=<?php echo $location ?>&page=<?php echo $i ?>">
               <?php echo $i ?></a>
               <?php }
                  }
                  } ?>
               <?php } else { ?>
                  <ul class="search-entry" type="none">
                     <?php echo "no result found"; ?>
                  </ul>
               <?php }
                  } else if((isset($_POST["provinces"]) && isset($_POST["activities"])) ||
                  (isset($_GET["province"]) && isset($_GET["activity"])) ) {
                    $api = '/v1/search?';
                    if(isset($_POST["provinces"]) && isset($_POST["activities"])) {
                      $province = $_POST["provinces"];
                      $activity = $_POST["activities"];
                    } else if(isset($_GET["province"]) && isset($_GET["activity"])) {
                      $province = $_GET["province"];
                      $activity = $_GET["activity"];
                    }
                    if("All" == $province && "All" == $activity) {
                      $api = '/v1/search';
                    if(isset($_GET["page"])) {
                      $api = $api . '?page=' . $_GET["page"];
                    }
                  } else if("All" != $province && "All" != $activity){
                    $api = $api . 'activity=' . $activity . '&province=' . $province;
                    if(isset($_GET["page"])) {
                      $api = $api . '&page=' . $_GET["page"];
                    }
                  } else if("All" != $province) {
                    $api = $api . 'province=' . $province;
                    if(isset($_GET["page"])) {
                      $api = $api . '&page=' . $_GET["page"];
                    }
                  } else if("All" != $activity) {
                    $api = $api . 'activity=' . $activity;
                    if(isset($_GET["page"])) {
                      $api = $api . '&page=' . $_GET["page"];
                    }
                  }
                  
                  $content = httpGet($host . $api);
                  $json = json_decode($content, true);
                  
                  if("true" == $json["success"]) { 
                    $imageHost = $json["image_host"] ?>
                  <ul class="search-entry" type="none">
                     <?php foreach ($json["results"] as $entry) { ?>
                     <li class="list-entry" type="none">
                        <a href="search-result.php?province=<?php echo $entry['province'] ?>&activity=<?php echo $entry['activity'] ?>&destination=<?php echo $entry['location_details'] ?>&location=<?php echo $entry['location_id'] ?>">
                           <table border="0" cellpadding="18px" cellspacing="10px" class="tbl-result" width="95%" margin="10px">
                              <tr nowrap>
                                 <td bgcolor="#ebf2ea" padding="20">
                                  <font color="#4d4d4d" size="5"><?php echo $entry["location_details"] ?></font>
                                  <br/>
                                  <font color="#4D4D4D" size="3"><?php echo $entry["entry_count"]; ?> tour guides</font>
                                 </td>
                              </tr>
                           </table>
                        </a>
                     </li>
                     <?php } ?>
                  </ul>
               <br>
               <?php echo "Pages: ";
                  $currentPage = $json["current_page"];
                  $totalPages = $json["total_pages"];
                  if(is_numeric($currentPage) && is_numeric($totalPages)) {
                    for($i=1; $i<=$totalPages; $i++) { ?>
               <?php if($i == $currentPage) {
                  echo $i;
                  } else { ?>
               <a href="search-result.php?province=<?php echo $province ?>&activity=<?php echo $activity ?>&page=<?php echo $i ?>">
               <?php echo $i ?></a>
               <?php }
                  }
                  } ?>
               <?php } else { ?>
                  <ul class="search-entry" type="none">
                     <?php echo "no result found"; ?>
                  </ul>
               <?php 
                  }
                  } ?>
            </td>
         </tr>
         <tr>
            <td align="center" bgcolor="#D4E034" colspan="2">
               <p>
                <font color="#4d4d4d">
                  &copy; 2015 Alpas
                  <br/>
                  <a href="index.php">Home</a> | <a href="about.php">About</a> | <a href="contact-us.php">Contact Us</a> | <a href="http://giya.voyager.ph">Full Version</a>
                </font>
               </p>
            </td>
         </tr>
      </table>
   </body>
   </head>
</html>