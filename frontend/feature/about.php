<!DOCTYPE html>
<html class="no-js" lang="en">
  <head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Giya | About</title>
    <link rel="stylesheet" type="text/css" href="/assets/css/normalize.css">
  </head>
  <body>
    <?php
    // Turn off all error reporting
    error_reporting(0);
    $host = "http://52.10.152.124:8000";
    function httpGet($url) {
      $ch = curl_init();
      curl_setopt($ch,CURLOPT_URL,$url);
      curl_setopt($ch,CURLOPT_RETURNTRANSFER,true);
      $output=curl_exec($ch);
      curl_close($ch);
      return $output;
    } ?>
    <table border="0" cellpadding="5" width="100%">
        <tr width="100%">
          <td align="center" bgcolor="#d4e034" width="90%">
            <p><center><a href="index.php"><img src="/assets/img/giya-logo.gif"></a></center></p>
          </td>
          <td align="right" bgcolor="#4d4d4d" width="10%">
            <p><center><a href="login.php"><img src="/assets/img/login.png"></a></center></p>
          </td>
        </tr>
        <tr>
          <td align="center" bgcolor="#f5faf4" colspan="2">
            <font color="#6d6e2e">
              <h3>ABOUT GIYA</h3>
              <p>'GIYA' IS A VISAYAN TERM<br/>THAT MEANS 'GUIDE'</p>
            </font>
            <font color="#4d4d4d">
              <p>Giya is a project that aims to connect travelers with local guides while eliminating the need for a middle man. Our objective is for tourists to skip travel agencies and go directly go to local guides for a richer and more personal experience.</p>
            </font>
          </td>
        </tr>
        <tr>
          <td align="center" bgcolor="#D4E034" colspan="2">
            <p>
              <font color="#4d4d4d">
                &copy; 2015 Alpas
                <br/>
                <a href="index.php">Home</a> | About | <a href="contact-us.php">Contact Us</a> | <a href="http://giya.voyager.ph">Full Version</a>
              </font>
            </p>
          </td>
        </tr>
    </table>
  </body>
</head>
</html>