<!doctype html>
<html class="no-js" lang="en">
  <head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>GIYA | Welcome</title>
    <link rel="shortcut icon" href="favicon.ico" type="image/x-icon">
    <link rel="icon" href="favicon.ico" type="image/x-icon">
    <link rel="stylesheet" href="/assets/css/normalize.css" />
    <link rel="stylesheet" href="/assets/css/foundation.css" />
    <link rel="stylesheet" href="/assets/css/styles.css" />
    <link rel='stylesheet prefetch' href='http://netdna.bootstrapcdn.com/font-awesome/3.2.1/css/font-awesome.css'>
    <script src="/assets/js/vendor/modernizr.js"></script>

  </head>
  <body>
    <?php
    // Turn off all error reporting
    error_reporting(0);
    $host = "http://52.10.152.124:8000";
    function httpGet($url) {
      $ch = curl_init();  
      curl_setopt($ch,CURLOPT_URL,$url);
      curl_setopt($ch,CURLOPT_RETURNTRANSFER,true);
      $output=curl_exec($ch);
      curl_close($ch);
      return $output;
    } ?>
    <div id="page">
      <div class="fullWidth giya-head">
        <div class="row">
          <a href="index.php"><img src="assets/img/giya-logo.png" class="giya-logo-2 home-logo"></a>
          <a href="#" class="open-panel"><i class="icon-reorder icon-2x"></i></a>
          <nav>
            <a href="#" class="close-panel"><i class="icon-remove-sign icon-large"></i></a>
              <ul class="menu">
                <li><a href="login.html" class="login-menu">Login</a></li>
                <li><a href="contact-us.php">Contact Us</a></li>
                <li><a href="form.php">Become a Guide</a></li>
                <li><a href="about.php">About</a></li>
              </ul>
          </nav>
        </div>
        <div class="row">
          <div class="columns small-12">
            <img src="assets/img/giya-mainlogo.png" class="main-logo">
            <h1>Where travelers and local guides meet.</h1>  
          </div>
        </div>
        <div class="row">          
          <form action="search-result.php" method="POST">
          <div class="columns large-4 medium-4 small-12">
            <select name="provinces" id="provinces">
              <?php if(!isset($_POST["provinces"]) || "All"==$_POST["provinces"]) { ?>
                <option selected="selected" value="All">Where do you want to go?</option>
              <?php }
              $content = httpGet($host . "/v1/provinces");
              $json = json_decode($content, true);

              foreach($json['result'] as $item) {
                if($item == $_POST["provinces"] || $item == $_GET["province"]) { ?>
                  <option selected="selected" value="<?php echo $item ?>"><?php echo $item ?></option>
                <?php } else { ?>
                  <option value="<?php echo $item ?>"><?php echo $item ?></option>
                <?php }
              } ?>
            </select>
          </div>
          <div class="columns large-4 medium-4 small-12">
            <select name="activities" id="activities">
              <?php if(!isset($_POST["activities"]) || "All"==$_POST["activities"]) { ?>
                <option selected="selected" value="All">What do you want to do?</option>
              <?php }
              $content = httpGet($host . "/v1/activities");
              $json = json_decode($content, true);
              foreach($json['result'] as $item) {
                if($item == $_POST["activities"] || $item == $_GET["activity"]) { ?>
                  <option selected="selected" value="<?php echo $item ?>"><?php echo $item ?></option>
                <?php } else { ?>
                  <option value="<?php echo $item ?>"><?php echo $item ?></option>
                <?php }
              } ?>
            </select>
          </div>
          <div class="columns large-4 medium-4 small-12">
            <input type="submit" value="FIND A GUIDE" class="submit-button">
          </div>
          </form>
        </div>
      </div>
      <div id="content">
        <div class="row explore-body">
          <div class="columns small-12">
            <h2>EXPLORE</h2>
            <h3>Go off the beaten path and see what else is out there.<br><br>
            <ul class="small-block-grid-1 medium-block-grid-3 large-block-grid-3">
              <li>
                <a href="search-result.php?province=Cebu&activity=Islands-Beaches&destination=Malapascua,%20Cebu&location=cebu-ib-2">
                  <div class="attraction-box malapascua">
                    <h1>MALAPASCUA</h1>
                    <h2>3 tour guides</h2>  
                  </div>
                </a>
              </li>
              <li>
                <a href="search-result.php?province=Ifugao&activity=Culture-Arts&destination=Batad,%20Ifugao&location=ifugao-ca-1">
                  <div class="attraction-box batad">
                    <h1>BATAD</h1>
                    <h2>3 tour guides</h2>  
                  </div>
                </a>
              </li>
              <li>
                <a href="search-result.php?province=Pangasinan&activity=Islands-Beaches&destination=Alaminos,%20Pangasinan&location=pangasinan-ib-2">
                  <div class="attraction-box alaminos">
                    <h1>ALAMINOS</h1>
                    <h2>3 tour guides</h2>  
                  </div>
                </a>
              </li>
            </ul>
          </div>
          <div class="columns small-12">
            <h2>EXPERIENCE</h2>
            <h3>Take a private tour with a knowleadgable local person as your tour guide!<br><br>
            <ul class="small-block-grid-1 medium-block-grid-3 large-block-grid-3">
              <li>
                <a href="search-result.php?province=ElNido&activity=Islands-Beaches&destination=San%20Vicente,%20El%20Nido&location=elnido-ib-2">
                  <div class="attraction-box sanvicente">
                    <h1>BEACH</h1>
                    <h2>3 tour guides</h2>  
                  </div>
                </a>
              </li>
              <li>
                <a href="search-result.php?province=ElNido&activity=Mountains-Waterfalls&destination=Taraw%20Cliff,%20El%20Nido&location=elnido-mw-1">
                  <div class="attraction-box osmena">
                    <h1>MOUNTAIN</h1>
                    <h2>3 tour guides</h2>  
                  </div>
                </a>
              </li>
              <li>
                <a href="search-result.php?province=Rizal&activity=Culture-Arts&destination=Angono,%20Rizal&location=rizal-ca-1">
                  <div class="attraction-box angono">
                    <h1>CULTURE</h1>
                    <h2>3 tour guides</h2>  
                  </div>
                </a>
              </li>
            </ul>
          </div>
        </div>
        <div class="columns small-12 local-ad">
          <h1>LOKAL KA BA?</h1>
          <h2>Mag-apply upang maging bahagi ng aming listahan ng mga gabay (tour guide).</h2>
          <div class="cta-buttons">
            <table align="center" border="0">
              <tr>
                <td><a href="form.php" class="submit-button-home">MAG-APPLY</a></td>
                <td><a href="login.html" class="submit-button-home">MAG-LOGIN</a></td>
              </tr>
            </table>
          </div>
        </div>
        <div class="fullWidth footer-giya">
          <div class="row">
            <div class="columns small-12">
            &copy; 2015 Alpas
              <ul class="footer-links">
                <li><a href="about.php">About</a></li>
                <li>|</li>
                <li><a href="contact-us.php">Contact&nbsp;Us</a></li>
              </ul>
          </div>
        </div>
      </div>
    </div>
    
    <script src="/assets/js/vendor/jquery.js"></script>
    <script src="/assets/js/foundation.min.js"></script>
    <script>
      $(document).foundation();

      $(".open-panel").click(function(){
  
        $("html").addClass("openNav");
        
      });
        
      $(".close-panel, #content").click(function(){
        
      $("html").removeClass("openNav");
        
      });
    </script>
  </body>
</html>
