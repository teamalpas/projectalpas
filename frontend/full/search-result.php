<!doctype html>
<html class="no-js" lang="en">
  <head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>GIYA | Welcome</title>
    <link rel="shortcut icon" href="favicon.ico" type="image/x-icon">
    <link rel="icon" href="favicon.ico" type="image/x-icon">
    <link rel="stylesheet" href="/assets/css/normalize.css" />
    <link rel="stylesheet" href="/assets/css/foundation.css" />
    <link rel="stylesheet" href="/assets/css/styles.css" />
    <link rel='stylesheet prefetch' href='http://netdna.bootstrapcdn.com/font-awesome/3.2.1/css/font-awesome.css'>
    <script src="assets/js/vendor/modernizr.js"></script>

  </head>
  <body>
    <?php
     // Turn off all error reporting
     error_reporting(0);
     $host = "http://52.10.152.124:8000";
     function httpGet($url) {
       $ch = curl_init();  
       curl_setopt($ch,CURLOPT_URL,$url);
       curl_setopt($ch,CURLOPT_RETURNTRANSFER,true);
       $output=curl_exec($ch);
       curl_close($ch);
       return $output;
     } ?>
    <div id="page">
      <div class="fullWidth giya-head giya-up">
        <div class="row">
        <a href="index.php"><img src="/assets/img/giya-logo.png" class="giya-logo-2"></a>
          <a href="#" class="open-panel"><i class="icon-reorder icon-2x"></i></a>
          <nav>
            <a href="#" class="close-panel"><i class="icon-remove-sign icon-large"></i></a>
              <ul class="menu">
                <li><a href="login.html" class="login-menu">Login</a></li>
                <li><a href="contact-us.php">Contact Us</a></li>
                <li><a href="form.php">Become a Guide</a></li>
                <li><a href="about.php">About</a></li>
              </ul>
          </nav>
        </div>
        <div class="row search-up">
        <form action="search-result.php" method="POST">
          <div class="columns large-4 medium-4 small-12">
            <select class="select-up" name="provinces" id="provinces">
                        <?php if(!isset($_POST["provinces"]) || "All"==$_POST["provinces"]) { ?>
                        <option selected="selected" value="All">Where do you want to go?</option>
                        <?php }
                           $content = httpGet($host . "/v1/provinces");
                           $json = json_decode($content, true);
                           
                           foreach($json['result'] as $item) {
                             if($item == $_POST["provinces"] || $item == $_GET["province"]) { ?>
                        <option selected="selected" value="<?php echo $item ?>"><?php echo $item ?></option>
                        <?php } else { ?>
                        <option value="<?php echo $item ?>"><?php echo $item ?></option>
                        <?php }
                           } ?>
                     </select>
          </div>
          <div class="columns large-4 medium-4 small-12">
            <select class="select-up" name="activities" id="activities">
                        <?php if(!isset($_POST["activities"]) || "All"==$_POST["activities"]) { ?>
                        <option selected="selected" value="All">What do you want to do?</option>
                        <?php }
                           $content = httpGet($host . "/v1/activities");
                           $json = json_decode($content, true);
                           foreach($json['result'] as $item) {
                             if($item == $_POST["activities"] || $item == $_GET["activity"]) { ?>
                        <option selected="selected" value="<?php echo $item ?>"><?php echo $item ?></option>
                        <?php } else { ?>
                        <option value="<?php echo $item ?>"><?php echo $item ?></option>
                        <?php }
                           } ?>
                     </select>
          </div>
          <div class="columns large-4 medium-4 small-12">
            <input type="submit" value="FIND A GUIDE" class="submit-button select-up">
          </div>
        </form>
        </div>
      </div>
      <div id="content">
        <div class="row result-body">
          <ol class="result-link">
            <li>
              <img src="/assets/img/mapmarker.png" class="map-marker">
            </li>
            <?php if(isset($_POST["provinces"])) { ?>
              <li><?php echo $_POST["provinces"]; ?> </li>
              <li><?php echo '>'; ?> </li>
            <?php } else if(isset($_GET["province"])) { ?>
              <li><?php echo $_GET["province"]; ?></li>
              <li><?php echo '>'; ?></li>
            <?php }
            if(isset($_POST["activities"])) { ?>
              <li><?php echo $_POST["activities"]; ?></li>
            <?php } else if(isset($_GET["activity"])) { ?>
              <li><?php echo $_GET["activity"]; ?></li>
            <?php }
            if(isset($_GET["destination"])) { ?>
              <li><?php echo '>'; ?></li>
              <li><?php echo $_GET["destination"]; ?></li>
            <?php } ?>
          </ol>
            <?php if(isset($_GET["guide"]) && isset($_GET["location"]) && isset($_GET["destination"]) && isset($_GET["province"]) && isset($_GET["activity"])) {
                  $province = $_GET["province"];
                  $activity = $_GET["activity"];
                  $destination = $_GET["destination"];
                  $location = $_GET["location"];
                  $guide = $_GET["guide"];
                  $api = '/v1/guides/' . $_GET["guide"];
                  $content = httpGet($host . $api);
                  $json = json_decode($content, true);
                  if("true" == $json["success"]) { 
                    $entry = $json["results"]; ?>
                    <div class="columns large-1 medium-1 small-12">&nbsp;</div>
                    <div class="columns large-10 medium-10 small-12 guide-profile">
                      <div class="row">
                        <div class="columns large-6 medium-6 small-12 border-right">
                          <img src="/assets/img/default-photo.png" class="guide-img">
                            <h2><img src="/assets/img/heart-red.png" class="heart-img-2"><span class="heart-count">23 people recommends this guide</span></h2>
                            <div class="phone-tab"><img src="/assets/img/icon-phone.png"><?php echo $entry["contact_details"]; ?></div>
                            <?php if(!empty($entry["facebook"])) { ?>
                            <a href="<?php echo $entry['facebook'] ?>"><div class="fb-tab">CONTACT VIA FACEBOOK</div></a>
                            <?php } ?>
                        </div>
                        <div class="columns large-6 medium-6 small-12 rel-pos">
                          <h1><?php echo $entry["complete_name"]; ?></h1>  
                          <h3><img src="/assets/img/mapmarker.png" class="marker-profile"><?php echo $destination . "," . $province; ?></h3>
                          <h4><span>Services include</span><br><br><?php echo $entry["full_description"]; ?></h4>  
                          <div class="guide-fee">
                            Guide Fee<br>
                            <span class="price-pax"><?php echo "P" . $entry["rate_details"]; ?></span><br>
                            <span class="price-text">*prices may vary depending on size of group.</span>
                          </div> 
                        </div>
                      </div>
                    </div>
                    <div class="columns large-1 medium-1 small-12">&nbsp;</div>
                  <?php }
            } else if(isset($_GET["location"]) && isset($_GET["destination"]) && isset($_GET["province"]) && isset($_GET["activity"])) { ?>
                  <?php $province = $_GET["province"];
                  $activity = $_GET["activity"];
                  $destination = $_GET["destination"];
                  $location = $_GET["location"];
                  $api = '/v1/locations/' . $_GET["location"];
                  if(isset($_GET["page"])) {
                    $api = $api . '?page=' . $_GET["page"];
                  }
                  $content = httpGet($host . $api);
                  $json = json_decode($content, true); ?>
                  <div class="large-8 medium-8 small-12 columns">
                    <img src="<?php echo $json["image_url"]; ?>" class="img-destination">
                  </div>
                  <div class="large-4 medium-4 small-12 columns">
                    <p class="text-desc"><?php echo $json["description"]; ?></p>
                  </div>
                  <div class="columns small-12">
                  <?php if("true" == $json["success"]) { ?>
                    <ul class="small-block-grid-1 medium-block-grid-3 large-block-grid-3">
                     <?php foreach ($json["results"] as $entry) { ?>
                        <li>
                          <a href="#">
                            <div class="guide-box">
                              <table width="100%" border="0" class="guide-tbl">
                                <tr>
                                  <td><img src="/assets/img/default-photosmall.png"></td>
                                  <td nowrap><h1><?php echo $entry["complete_name"]; ?></h1></td>
                                  <td nowrap><span class="heart-count">23v</span><img src="/assets/img/heart-red.png" class="heart-img"></td>
                                </tr>
                              </table>
                              <hr align="center" width="90%" style="margin: -7px auto; margin-bottom: 10px;">
                              <table width="100%" border="0" class="guide-tbl">
                                <tr>
                                  <td><h2><?php echo $entry["service_description"]; ?></h2></td>
                                  <td><span class="guide-price">P<?php echo $entry["rate_details"]; ?></span></td>
                                </tr>
                              </table>
                              <a href="search-result.php?province=<?php echo $province ?>&activity=<?php echo $activity ?>&destination=<?php echo $destination ?>&location=<?php echo $location ?>&guide=<?php echo $entry['guide_id'] ?>">
                                <div class="guide-link">VIEW DETAILS</div>
                              </a>
                            </div>
                          </a>
                        </li>    
                     <?php } ?>
                   </ul>
               <?php
                  $currentPage = $json["current_page"];
                  $totalPages = $json["total_pages"];
                  if(is_numeric($currentPage) && is_numeric($totalPages)) { ?>
                    <ol class="paging-links">
                    <?php for($i=1; $i<=$totalPages; $i++) { ?>
                      <?php if($i == $currentPage) { ?>
                        <li><?php echo $i; ?></li>
                      <?php } else { ?>
                        <li><a href="search-result.php?province=<?php echo $province ?>&activity=<?php echo $activity ?>&destination=<?php echo $destination ?>&location=<?php echo $location ?>&page=<?php echo $i ?>">
                        <?php echo $i ?></a></li>
                      <?php }
                    } ?>
                    </ol>
                  <?php } ?>
                <?php } else { ?>
                  <li>
                    <a href="#">
                      <div class="result-box bantayan">
                        <h1>No Result Found</h1>
                      </div>
                    </a>
                  </li>
                <?php 
              } ?>
              </div>
            <?php } else if((isset($_POST["provinces"]) && isset($_POST["activities"])) ||
                  (isset($_GET["province"]) && isset($_GET["activity"])) ) {
                    $api = '/v1/search?';
                    if(isset($_POST["provinces"]) && isset($_POST["activities"])) {
                      $province = $_POST["provinces"];
                      $activity = $_POST["activities"];
                    } else if(isset($_GET["province"]) && isset($_GET["activity"])) {
                      $province = $_GET["province"];
                      $activity = $_GET["activity"];
                    }
                    if("All" == $province && "All" == $activity) {
                      $api = '/v1/search';
                    if(isset($_GET["page"])) {
                      $api = $api . '?page=' . $_GET["page"];
                    }
                  } else if("All" != $province && "All" != $activity){
                    $api = $api . 'activity=' . $activity . '&province=' . $province;
                    if(isset($_GET["page"])) {
                      $api = $api . '&page=' . $_GET["page"];
                    }
                  } else if("All" != $province) {
                    $api = $api . 'province=' . $province;
                    if(isset($_GET["page"])) {
                      $api = $api . '&page=' . $_GET["page"];
                    }
                  } else if("All" != $activity) {
                    $api = $api . 'activity=' . $activity;
                    if(isset($_GET["page"])) {
                      $api = $api . '&page=' . $_GET["page"];
                    }
                  }
                  $content = httpGet($host. $api);
                  $json = json_decode($content, true); ?>
                  <div class="columns small-12">
                  <?php if("true" == $json["success"]) { 
                    $imageHost = $json["image_host"] ?>
                    <ul class="small-block-grid-1 medium-block-grid-3 large-block-grid-3">
                    <?php foreach ($json["results"] as $entry) { ?>
                    <li>
                        <a href="search-result.php?province=<?php echo $entry['province'] ?>&activity=<?php echo $entry['activity'] ?>&destination=<?php echo $entry['location_details'] ?>&location=<?php echo $entry['location_id'] ?>">
                          <div class="label-row">
                            <div class="label-row-inside">
                              <?php if($entry["activity"] == 'Islands-Beaches') { ?>
                                <img src="/assets/img/icon-sea.png" class="cat-icon">
                              <?php } else if($entry["activity"] == 'Mountains-Waterfalls') { ?>
                                <img src="/assets/img/icon-summit.png" class="cat-icon">
                              <?php } else if($entry["activity"] == 'Culture-Arts') { ?>
                                <img src="/assets/img/icon-art.png" class="cat-icon">
                              <?php } else if($entry["activity"] == 'Heritage-History') { ?>
                                <img src="/assets/img/icon-history.png" class="cat-icon">
                              <?php } ?>
                              <h1><?php echo $entry["location_details"] ?></h1>
                              <h2><?php echo $entry["entry_count"]; ?> tour guides</h2> 
                            </div>
                            <!-- <img src="/assets/img/attraction/malapascua.jpg"> -->
                            <img src="<?php echo $imageHost . $entry["image_url"]; ?>">
                          </div>
                        </a>
                    </li>
                    <?php } ?>
                    </ul>
               <?php
                  $currentPage = $json["current_page"];
                  $totalPages = $json["total_pages"];
                  if(is_numeric($currentPage) && is_numeric($totalPages)) { ?>
                    <ol class="paging-links">
                    <?php for($i=1; $i<=$totalPages; $i++) { ?>
                      <?php if($i == $currentPage) { ?>
                        <li><?php echo $i; ?></li>
                      <?php } else { ?>
                        <li><a href="search-result.php?province=<?php echo $province ?>&activity=<?php echo $activity ?>&page=<?php echo $i ?>">
                        <?php echo $i ?></a></li>
                      <?php }
                    } ?>
                    </ol>
                  <?php } ?>
                <?php } else { ?>
                  <li>
                    <a href="#">
                      <div class="result-box bantayan">
                        <h1>No Result Found</h1>
                      </div>
                    </a>
                  </li>
                <?php 
              } ?>
            </div>
            <?php } ?>
        </div>
        <div class="fullWidth footer-giya">
          <div class="row">
            <div class="columns small-12">
            &copy; 2015 Alpas
              <ul class="footer-links">
                <li><a href="about.php">About</a></li>
                <li>|</li>
                <li><a href="contact-us.php">Contact&nbsp;Us</a></li>
              </ul>
          </div>
        </div>
      </div>
    </div>
    
    <script src="/assets/js/vendor/jquery.js"></script>
    <script src="/assets/js/foundation.min.js"></script>
    <script>
      $(document).foundation();

      $(".open-panel").click(function(){
  
        $("html").addClass("openNav");
        
      });
        
      $(".close-panel, #content").click(function(){
        
      $("html").removeClass("openNav");
        
      });
    </script>
  </body>
</html>
