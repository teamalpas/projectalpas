<!doctype html>
<html class="no-js" lang="en">
  <head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>GIYA | Welcome</title>
    <link rel="shortcut icon" href="favicon.ico" type="image/x-icon">
    <link rel="icon" href="favicon.ico" type="image/x-icon">
    <link rel="stylesheet" href="/assets/css/normalize.css" />
    <link rel="stylesheet" href="/assets/css/foundation.css" />
    <link rel="stylesheet" href="/assets/css/styles.css" />
    <link rel='stylesheet prefetch' href='http://netdna.bootstrapcdn.com/font-awesome/3.2.1/css/font-awesome.css'>
    <script src="/assets/js/vendor/modernizr.js"></script>

  </head>
  <body>
    <?php
    // Turn off all error reporting
    error_reporting(0);
    $host = "http://52.10.152.124:8000";
    function httpPost($url,$params) {
    $ch = curl_init();  
      curl_setopt($ch,CURLOPT_URL,$url);
      curl_setopt($ch,CURLOPT_RETURNTRANSFER,true);
      curl_setopt($ch,CURLOPT_HEADER, false); 
      curl_setopt($ch, CURLOPT_POSTFIELDS, $params);    
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);                                                                      
      curl_setopt($ch, CURLOPT_HTTPHEADER, array(                                                                          
        'Content-Type: application/json',                                                                                
        'Content-Length: ' . strlen($params)));
      $output=curl_exec($ch);
      curl_close($ch);
      return $output;
  } ?>
    <div id="page">
      <div class="fullWidth giya-head giya-up">
        <div class="row">
        <a href="index.php"><img src="/assets/img/giya-logo.png" class="giya-logo-2"></a>
          <a href="#" class="open-panel"><i class="icon-reorder icon-2x"></i></a>
          <nav>
            <a href="#" class="close-panel"><i class="icon-remove-sign icon-large"></i></a>
              <ul class="menu">
                <li><a href="login.html" class="login-menu">Login</a></li>
                <li><a href="contact-us.php">Contact Us</a></li>
                <li><a href="form.php">Become a Guide</a></li>
                <li><a href="about.php">About</a></li>
              </ul>
          </nav>
        </div>
      </div>
      <div id="content">
        <div class="row form-body">
          <?php
            // Turn off all error reporting
            error_reporting(0);
            if(isset($_POST["name"]) && isset($_POST["contact_info"]) && isset($_POST["street"]) && isset($_POST["city"]) && isset($_POST["province"]) && isset($_POST["service"])) {
              if("" != $_POST["name"] && "" != $_POST["contact_info"] && "" != $_POST["street"] && "" != $_POST["city"] && "" != $_POST["province"] && "" != $_POST["service"]) {
                // submit form
                $jsonBuilder = array('guide_name' => $_POST["name"], 'contact_info' => $_POST["contact_info"], 'street' => $_POST["street"], 'city' => $_POST["city"], 'province' => $_POST["province"], 'service' => $_POST["service"]);
                $data = json_encode($jsonBuilder);
              
                $options = array(
                  'http' => array(
                  'header'  => "Content-type: application/json",
                    'method'  => 'POST',
                    'content' => $data,
                  ),
                );
                $result = httpPost($host . "/v1/application",$data);

                if($result["success"] == true) {
                  $hasSubmitted = true; ?>
                  <div class="columns small-12 success-message displaly-nothing">
                    Form Successfully Submitted. Thank you!
                  </div>
                <?php } else { 
                  $hasSubmitted = false; ?>
                  <div class="columns small-12 warning-message displaly-nothing">
                    <h2> Application failed to send, please try again. </h2>
                  </div>
                  <div class="columns small-12">
                <?php }
              } else { 
                  $hasSubmitted = false; ?>
                <div class="columns small-12 warning-message displaly-nothing">
                  <h2> Please fill up all the fields in the form </h2>
                </div>
                <div class="columns small-12">
              <?php }
            } else { 
              $hasSubmitted = false; ?>
              <div class="columns small-12">
              <h2>Kung nais mong mailista ang iyong pangalan at impormasyon bilang isang gabay/tour guide sa website na ito, punan ang mga detalyeng hinihingi sa baba.</h2>  
            <?php }
            if($hasSubmitted == false) { ?>
              <form action="form.php" method="POST">
                <?php if($hasSubmitted == false && isset($_POST["name"])) { ?>
                  <input type="text" class="textbox" name="name" value="<?php echo $_POST['name'] ?>" placeholder="Name">
                <?php } else { ?>
                  <input type="text" class="textbox" name="name" placeholder="Name">
                <?php } ?>
                <?php if($hasSubmitted == false && isset($_POST["contact_info"])) { ?>
                  <input type="text" class="textbox" name="contact_info" value="<?php echo $_POST['contact_info'] ?>" placeholder="Contact Info">
                <?php } else { ?>
                  <input type="text" class="textbox" name="contact_info" placeholder="Contact Info">
                <?php } ?>
                <?php if($hasSubmitted == false && isset($_POST["street"])) { ?>
                  <input type="text" class="textbox" name="street" value="<?php echo $_POST['street'] ?>" placeholder="Street">
                <?php } else { ?>
                  <input type="text" class="textbox" name="street" placeholder="Street">
                <?php } ?>
                <?php if($hasSubmitted == false && isset($_POST["city"])) { ?>
                  <input type="text" class="textbox" name="city" value="<?php echo $_POST['city'] ?>" placeholder="City">
                <?php } else { ?>
                  <input type="text" class="textbox" name="city" placeholder="City">
                <?php } ?>
                <?php if($hasSubmitted == false && isset($_POST["province"])) { ?>
                  <input type="text" class="textbox" name="province" value="<?php echo $_POST['province'] ?>" placeholder="Province">
                <?php } else { ?>
                  <input type="text" class="textbox" name="province" placeholder="Province">
                <?php } ?>
                <?php if($hasSubmitted == false && isset($_POST["service"])) { ?>
                  <textarea rows="4" cols="50" name="service" class="textbox" placeholder="Service"><?php echo $_POST['service'] ?></textarea>
                <?php } else { ?>
                  <textarea rows="4" cols="50" name="service" class="textbox" placeholder="Service"></textarea>
                <?php } ?>
                <input type="submit" name="SUBMIT" value="SUBMIT" class="submit-button">
              </form>
            </div>
          <?php } ?>
        </div>
        <div class="fullWidth footer-giya">
          <div class="row">
            <div class="columns small-12">
            &copy; 2015 Alpas
              <ul class="footer-links">
                <li><a href="about.php">About</a></li>
                <li>|</li>
                <li><a href="contact-us.php">Contact&nbsp;Us</a></li>
              </ul>
          </div>
        </div>
      </div>
    </div>
    
    <script src="/assets/js/vendor/jquery.js"></script>
    <script src="/assets/js/foundation.min.js"></script>
    <script>
      $(document).foundation();

      $(".open-panel").click(function(){
  
        $("html").addClass("openNav");
        
      });
        
      $(".close-panel, #content").click(function(){
        
      $("html").removeClass("openNav");
        
      });
    </script>
  </body>
</html>
