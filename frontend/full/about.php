<!doctype html>
<html class="no-js" lang="en">
  <head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>GIYA | Welcome</title>
    <link rel="shortcut icon" href="favicon.ico" type="image/x-icon">
    <link rel="icon" href="favicon.ico" type="image/x-icon">
    <link rel="stylesheet" href="/assets/css/normalize.css" />
    <link rel="stylesheet" href="/assets/css/foundation.css" />
    <link rel="stylesheet" href="/assets/css/styles.css" />
    <link rel='stylesheet prefetch' href='http://netdna.bootstrapcdn.com/font-awesome/3.2.1/css/font-awesome.css'>
    <script src="/assets/js/vendor/modernizr.js"></script>

  </head>
  <body>

    <div id="page">
      <div class="fullWidth giya-head giya-up">
        <div class="row">
        <a href="index.php"><img src="/assets/img/giya-logo.png" class="giya-logo-2"></a>
          <a href="#" class="open-panel"><i class="icon-reorder icon-2x"></i></a>
          <nav>
            <a href="#" class="close-panel"><i class="icon-remove-sign icon-large"></i></a>
              <ul class="menu">
                <li><a href="login.html" class="login-menu">Login</a></li>
                <li><a href="contact-us.php">Contact Us</a></li>
                <li><a href="form.php">Become a Guide</a></li>
                <li><a href="about.php">About</a></li>
              </ul>
          </nav>
        </div>
      </div>
      <div id="content">
        <div class="fullWidth about-body">
          <div class="row">
            <div class="columns large-7 medium-12 small-12 overlay-content">
              <div class="row">
                <div class="columns large-12 content-about">
                  <h1>ABOUT GIYA</h1> 
                  <h2>'GIYA' IS A VISAYAN TERM<br>THAT MEANS 'GUIDE'</h2> 
                  <h3>Giya is a project that aims to connect travelers with local guides while eliminating the need for a middle man. Our objective is for tourist to skip travel agencies and go directly to local guides for a richer and more personal experience.</h3>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="fullWidth footer-giya">
          <div class="row">
            <div class="columns small-12">
            &copy; 2015 Alpas
              <ul class="footer-links">
                <li><a href="about.php">About</a></li>
                <li>|</li>
                <li><a href="contact-us.php">Contact&nbsp;Us</a></li>
              </ul>
          </div>
        </div>
      </div>
    </div>
    
    <script src="/assets/js/vendor/jquery.js"></script>
    <script src="/assets/js/foundation.min.js"></script>
    <script>
      $(document).foundation();

      $(".open-panel").click(function(){
  
        $("html").addClass("openNav");
        
      });
        
      $(".close-panel, #content").click(function(){
        
      $("html").removeClass("openNav");
        
      });
    </script>
  </body>
</html>
