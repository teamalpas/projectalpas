<!DOCTYPE html>
<html>
<body>
<?php
// Turn off all error reporting
error_reporting(0);
if(isset($_POST["name"]) && isset($_POST["contact_info"]) && isset($_POST["street"]) && isset($_POST["city"]) && isset($_POST["province"]) && isset($_POST["service"])) {
	if("" != $_POST["name"] && "" != $_POST["contact_info"] && "" != $_POST["street"] && "" != $_POST["city"] && "" != $_POST["province"] && "" != $_POST["service"]) {
		// submit form
		$jsonBuilder = array('guide_name' => $_POST["name"], 'contact_info' => $_POST["contact_info"], 'street' => $_POST["street"], 'city' => $_POST["city"], 'province' => $_POST["province"], 'service' => $_POST["service"]);
		$data = json_encode($jsonBuilder);
		$url = 'http://localhost:8000/v1/application';
		
		$options = array(
    		'http' => array(
    		'header'  => "Content-type: application/json",
        	'method'  => 'POST',
        	'content' => $data,
			),
		);
		$context  = stream_context_create($options);
		$result = file_get_contents($url, false, $context);

		if($result["success"] == true) {
			$hasSubmitted = true; ?>
			<h1> Application sent successfully. </h1>
		<?php } else { ?>
			$hasSubmitted = false;
			<h1> Application failed to send, please try again. </h1>
		<?php }
	} else { ?>
		<h1> Please fill up all the fields in the form </h1>
		<?php $hasSubmitted = false;
	}
} ?>
<form action="submission-form.php" method="POST">
<b>Name:</b>
<br>
<?php if($hasSubmitted == false && isset($_POST["name"])) { ?>
	<input type="text" name="name" value="<?php echo $_POST['name'] ?>">
<?php } else { ?>
	<input type="text" name="name">
<?php } ?>
<br>
<b>Contact Info:</b>
<br>
<?php if($hasSubmitted == false && isset($_POST["contact_info"])) { ?>
	<input type="text" name="contact_info" value="<?php echo $_POST['contact_info'] ?>">
<?php } else { ?>
	<input type="text" name="contact_info">
<?php } ?>
<br>
<b>Street:</b>
<br>
<?php if($hasSubmitted == false && isset($_POST["street"])) { ?>
	<input type="text" name="street" value="<?php echo $_POST['street'] ?>">
<?php } else { ?>
	<input type="text" name="street">
<?php } ?>
<br>
<b>City:</b>
<br>
<?php if($hasSubmitted == false && isset($_POST["city"])) { ?>
	<input type="text" name="city" value="<?php echo $_POST['city'] ?>">
<?php } else { ?>
	<input type="text" name="city">
<?php } ?>
<br>
<b>Province:</b>
<br>
<?php if($hasSubmitted == false && isset($_POST["province"])) { ?>
	<input type="text" name="province" value="<?php echo $_POST['province'] ?>">
<?php } else { ?>
	<input type="text" name="province">
<?php } ?>
<br>
<b>Service:</b>
<br>
<?php if($hasSubmitted == false && isset($_POST["service"])) { ?>
	<input type="text" name="service" value="<?php echo $_POST['service'] ?>">
<?php } else { ?>
	<input type="text" name="service">
<?php } ?>
<br>
<br>
<input type="submit" value="Submit Application Form">
</form>
</body>
</html>
<!-- {
 "guide_name": "Giselle Palo",
 "contact_info": "+639470000002",
 "street": "Villa Olympia village",
 "city": "San Pedro",
 "province": "Laguna",
 "service": "Shopping"
} -->