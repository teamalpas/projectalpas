<!DOCTYPE html>
<html>
<body>
<?php
// Turn off all error reporting
error_reporting(0);
?>
<b>Provinces:</b>
<br>
<form action="alpas.php" method="POST">
<select name="provinces" id="provinces">
	<?php if(!isset($_POST["provinces"]) || "All"==$_POST["provinces"]) { ?>
    	<option selected="selected" value="All">Where do you want to go?</option>
	<?php }
$url = 'http://localhost:8000/v1/provinces';
$content = file_get_contents($url);
$json = json_decode($content, true);

foreach($json['result'] as $item) {
	if($item == $_POST["provinces"] || $item == $_GET["province"]) { ?>
    <option selected="selected" value="<?php echo $item ?>"><?php echo $item ?></option>
	<?php } else { ?>
    <option value="<?php echo $item ?>"><?php echo $item ?></option>
	<?php }
} ?>
</select>
<br>
<b>Activities:</b>
<br>
<select name="activities" id="activities">
	<?php if(!isset($_POST["activities"]) || "All"==$_POST["activities"]) { ?>
    	<option selected="selected" value="All">What do you want to do?</option>
	<?php }
$url = 'http://localhost:8000/v1/activities';
$content = file_get_contents($url);
$json = json_decode($content, true);

foreach($json['result'] as $item) {
	if($item == $_POST["activities"] || $item == $_GET["activity"]) { ?>
    <option selected="selected" value="<?php echo $item ?>"><?php echo $item ?></option>
	<?php } else { ?>
    <option value="<?php echo $item ?>"><?php echo $item ?></option>
	<?php }
} ?>
</select>
<br>
<input type="submit" value="FIND A GUIDE">
</form>
<br>
<?php
if(isset($_POST["provinces"])) {
	echo $_POST["provinces"];
	echo ' -> ';
} else if(isset($_GET["province"])) {
	echo $_GET["province"];
	echo ' -> ';
}
if(isset($_POST["activities"])) {
	echo $_POST["activities"];
} else if(isset($_GET["activity"])) {
	echo $_GET["activity"];
}
if(isset($_GET["destination"])) {
	echo ' -> ';
	echo $_GET["destination"];
} ?>
<br>
<?php
if(isset($_GET["location"]) && isset($_GET["destination"]) && isset($_GET["province"]) && isset($_GET["activity"])) {
	if(isset($_GET["province"]) && isset($_GET["activity"])) {
		$province = $_GET["province"];
		$activity = $_GET["activity"];
		$destination = $_GET["destination"];
		$location = $_GET["location"];
	}
	$url = 'http://localhost:8000/v1/' . $_GET["location"];
	if(isset($_GET["page"])) {
		$url = $url . '?page=' . $_GET["page"];
	}
	$content = file_get_contents($url);
	$json = json_decode($content, true);
	if("true" == $json["success"]) {
		foreach ($json["results"] as $entry) { ?>
		<br>
		<?php 
	   		echo $entry["complete_name"]; ?>
	   		<br>
	   		<?php echo $entry["contact_details"]; ?>
	   		<br>
	   		<?php echo $entry["rate_details"]; ?>
	   		<br>
	   		<?php echo $entry["service_description"]; ?>
	   		<br>
		<?php } ?>
		<br>
		<?php 
		echo "Pages: ";
		$currentPage = $json["current_page"];
		$totalPages = $json["total_pages"];
		if(is_numeric($currentPage) && is_numeric($totalPages)) {
			for($i=1; $i<=$totalPages; $i++) { ?>
				<br>
				<?php if($i == $currentPage) {
					echo $i;
				} else { ?>
					<a href="http://localhost/alpas.php?province=<?php echo $province ?>&activity=<?php echo $activity ?>&destination=<?php echo $destination ?>&location=<?php echo $location ?>&page=<?php echo $i ?>">
					<?php echo $i ?></a>
				<?php }
			 }
		}
	} else {
		echo "no result found";
	}
} else if((isset($_POST["provinces"]) && isset($_POST["activities"])) ||
	(isset($_GET["province"]) && isset($_GET["activity"])) ) {
	$url = 'http://localhost:8000/v1/search?';
	if(isset($_POST["provinces"]) && isset($_POST["activities"])) {
		$province = $_POST["provinces"];
		$activity = $_POST["activities"];
	} else if(isset($_GET["province"]) && isset($_GET["activity"])) {
		$province = $_GET["province"];
		$activity = $_GET["activity"];
	}
	if("All" == $province && "All" == $activity) {
		$url = 'http://localhost:8000/v1/search';
		if(isset($_GET["page"])) {
		$url = $url . '?page=' . $_GET["page"];
		}
	} else if("All" != $province && "All" != $activity){
		$url = $url . 'activity=' . $activity . '&province=' . $province;
		if(isset($_GET["page"])) {
		$url = $url . '&page=' . $_GET["page"];
		}
	} else if("All" != $province) {
		$url = $url . 'province=' . $province;
		if(isset($_GET["page"])) {
		$url = $url . '&page=' . $_GET["page"];
		}
	} else if("All" != $activity) {
		$url = $url . 'activity=' . $activity;
		if(isset($_GET["page"])) {
		$url = $url . '&page=' . $_GET["page"];
		}
	}
	
	$content = file_get_contents($url);
	$json = json_decode($content, true);

	if("true" == $json["success"]) {

		foreach ($json["results"] as $entry) { ?>
		<br>
		<a href="http://localhost/alpas.php?province=<?php echo $entry['province'] ?>&activity=<?php echo $entry['activity'] ?>&destination=<?php echo $entry['location_details'] ?>&location=<?php echo $entry['location_id'] ?>">
		<?php echo $entry["location_details"] ?> 
		</a>
		<?php 
	   		echo '(' . $entry["entry_count"] . ')'; ?>
	   		<br>
	   		<?php echo $entry["province"]; ?>
	   		<br>
	   		<?php echo $entry["activity"]; ?>
	   		<br>
		<?php } ?>
		<br>
		<?php 
		echo "Pages: ";
		$currentPage = $json["current_page"];
		$totalPages = $json["total_pages"];
		if(is_numeric($currentPage) && is_numeric($totalPages)) {
			for($i=1; $i<=$totalPages; $i++) { ?>
				<br>
				<?php if($i == $currentPage) {
					echo $i;
				} else { ?>
					<a href="http://localhost/alpas.php?province=<?php echo $entry['province'] ?>&activity=<?php echo $entry['activity'] ?>&page=<?php echo $i ?>">
					<?php echo $i ?></a>
				<?php }
			 }
		}
	} else {
		echo "no result found";
	}
} ?>

</body>
</html>