#!/bin/bash

RED=$(tput setaf 1)
GREEN=$(tput setaf 2)
NC=$(tput sgr0)

### CONFIG DETAILS
echo "${RED}NOTE:${NC} Make sure to double check the MySQL credentials used by this script. Please update accordingly..."
MYSQL_USER="giya"
MYSQL_PASS="giya"
MYSQL_DB="giya"
MYSQL_DUMP="$(pwd)/alpas-giya.sql"

echo "${RED}NOTE:${NC} run ${GREEN}./deploy.sh restart${NC} if you have to reset the DB or if you have DB changes..."
if [[ "$1" == "restart" ]]
	then
	### Forcefully Drop and Create Database
	mysql -u$MYSQL_USER -p$MYSQL_PASS -e "DROP DATABASE $MYSQL_DB" &> /dev/null
	mysql -u$MYSQL_USER -p$MYSQL_PASS -e "CREATE DATABASE $MYSQL_DB" &> /dev/null

	### Rebuild DB contents 
	mysql -u$MYSQL_USER -p$MYSQL_PASS $MYSQL_DB < $MYSQL_DUMP &> /dev/null

	### Update local.js
	echo "${RED}NOTE:${NC} Make sure to double check the config file (${GREEN}config/local.js${NC}) for the proper configuration. For now, the template (local.js.sample) will be followed..."
	CONFIG_PATH="$(pwd)/../config/"
	cp $CONFIG_PATH/local.js.sample $CONFIG_PATH/local.js
fi

echo "${RED}NOTE:${NC} If you have any config changes, run ${GREEN}./deploy.sh restart${NC}"
echo ""

### Run app
cd ..
node giya.js

