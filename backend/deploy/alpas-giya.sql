-- MySQL dump 10.13  Distrib 5.6.22, for osx10.10 (x86_64)
--
-- Host: localhost    Database: alpas-giya
-- ------------------------------------------------------
-- Server version 5.6.22

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `activities`
--

DROP TABLE IF EXISTS `activities`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `activities` (
  `activity` varchar(255) DEFAULT NULL,
  UNIQUE KEY `activity` (`activity`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `activities`
--

LOCK TABLES `activities` WRITE;
/*!40000 ALTER TABLE `activities` DISABLE KEYS */;
INSERT INTO `activities` VALUES ('Adventure'), ('Beach'), ('Culture'), ('Museum'), ('Shopping'), ('Services');
/*!40000 ALTER TABLE `activities` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `applicants`
--

DROP TABLE IF EXISTS `applicants`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `applicants` (
  `guide_id` varchar(255) DEFAULT NULL,
  `complete_name` varchar(255) DEFAULT NULL,
  `contact_details` varchar(255) DEFAULT NULL,
  `rate_details` varchar(255) DEFAULT NULL,
  `service_description` varchar(255) DEFAULT NULL,
  `address` varchar(255) DEFAULT NULL,
  `facebook` varchar(255) DEFAULT NULL,
  `status` varchar(255) DEFAULT NULL,
  `createdAt` datetime DEFAULT NULL,
  UNIQUE KEY `guide_id` (`guide_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `applicants`
--

LOCK TABLES `applicants` WRITE;
/*!40000 ALTER TABLE `applicants` DISABLE KEYS */;
/*!40000 ALTER TABLE `applicants` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `guides`
--

DROP TABLE IF EXISTS `guides`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `guides` (
  `location_id` varchar(255) DEFAULT NULL,
  `guide_id` varchar(255) DEFAULT NULL,
  `complete_name` varchar(255) DEFAULT NULL,
  `contact_details` varchar(255) DEFAULT NULL,
  `rate_details` varchar(255) DEFAULT NULL,
  `service_description` varchar(255) DEFAULT NULL,
  `address` varchar(255) DEFAULT NULL,
  `facebook` varchar(255) DEFAULT NULL,
  `createdAt` datetime DEFAULT NULL,
  UNIQUE KEY `guide_id` (`guide_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `guides`
--

LOCK TABLES `guides` WRITE;
/*!40000 ALTER TABLE `guides` DISABLE KEYS */;
INSERT INTO `guides` VALUES 
  ('pangasinan-adventure-1', 'pangasinan-adventure-1-abcde00001', 'Kuya Julius', '+639464540179', 'P350~ per person', 
    'Services include tricycle ride around Pangasinan: Pickup from St. James the Great Parish to accomodation and drop off to bus terminal; Tour from Bolinao lighthouse, Rock View beach, Wonderful Cave, Bolinao Falls 1 & 2.',
    'Bolinao, Pangasinan', NULL, '2015-05-28 10:36:19'),
  ('bulacan-adventure-1', 'bulacan-adventure-1-abcde00001', 'Aileen and Shella (sisters)', NULL, 'No standard rate', 
    'Guide from ascending to descending of both Lioness Back and Rhino rock formations.',
    'Bigte, Norzagaray, Bulacan', NULL, '2015-05-28 14:12:53'),
  ('aurora-adventure-1', 'aurora-adventure-1-abcde00001', 'Kuya Rowell', NULL, 'No standard rate', 
    'June\'s Homestay.',
    'Baler, Aurora', 'http://www.facebook.com/robbie.bautista', '2015-05-29 17:32:10');
/*!40000 ALTER TABLE `guides` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `province_activity_map`
--

DROP TABLE IF EXISTS `province_activity_map`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `province_activity_map` (
  `province` varchar(255) DEFAULT NULL,
  `activity` varchar(255) DEFAULT NULL,
  `location_id` varchar(255) DEFAULT NULL,
  `location_details` varchar(255) DEFAULT NULL,
  `entry_count` int(11) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `image_url` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `province_activity_map`
--

LOCK TABLES `province_activity_map` WRITE;
/*!40000 ALTER TABLE `province_activity_map` DISABLE KEYS */;
INSERT INTO `province_activity_map` VALUES 
  ('Pangasinan','Adventure', 'pangasinan-adventure-1', 'Bolinao, Pangasinan - Bolinao Falls 1 & 2', 1, 
    'Bolinao Falls 1 & 2 are located in Samang Norte, Bolinao Trail.', 
    'pangasinan-adventure-1.jpg'),
  ('Bulacan','Adventure', 'bulacan-adventure-1', 'Bigte, Norzagaray, Bulacan - Lioness Back and Rhino Rock Formations', 1, 
    'Just two hours away from Manila, you can experience rock scrambling these two majestic rock formations. With a difficulty rating of 4/5, these two rock formations surely will add a little excitement in your life (remember to be really, really, really cautious!).', 
    'bulacan-adventure-1.jpg'),
  ('Tañon Strait','Beach', 'tanon-beach-1', 'Bantayan Island, Tañon Strait', 0, 
    'White sand beach located in the Visayan Sea. Good place for rest and relaxation especially if you want to be alone.', 
    'tanon-beach.jpg'),
  ('Aurora','Beach', 'aurora-beach-1', 'Baler Aurora - Sabang Beach', 0, 
    'Popular among tourists as a surfing site.', 
    'aurora-beach-1.jpg'),
  ('Aurora','Adventure', 'aurora-adventure-1', 'Baler, Aurora - Diguisit Falls', 1, 
    'Falls beside the road. Eight minutes or less of clambering up a muddy/rocky (depends on the weather) soil to reach the falls. No actual pool where you can swim but the water is really clean.', 
    'aurora-adventure-1.jpg'),
  ('Aurora','Adventure', 'aurora-adventure-2', 'Baler, Aurora - Lukso-lukso Islet', 0, 
    'This island showcases rocks of different sizes and colors as well as rock formations that you can climb (at your own risk!). Let the sand, water, and rocks touch your feet as you relax to the sound of the waves crashing against the big rock formations.', 
    'aurora-adventure-2.jpg'),
  ('Aurora','Adventure', 'aurora-adventure-3', 'San Luis, Aurora - Ditumabo Mother Falls', 0, 
    '30-minute trek to falls.', 
    'aurora-adventure-3.jpg');
/*!40000 ALTER TABLE `province_activity_map` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `provinces`
--

DROP TABLE IF EXISTS `provinces`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `provinces` (
  `province` varchar(255) DEFAULT NULL,
  UNIQUE KEY `province` (`province`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `provinces`
--

LOCK TABLES `provinces` WRITE;
/*!40000 ALTER TABLE `provinces` DISABLE KEYS */;
INSERT INTO `provinces` VALUES ('Aurora'), ('Bulacan'), ('Pangasinan'), ('Tañon Strait');
/*!40000 ALTER TABLE `provinces` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2015-05-29 15:10:43
