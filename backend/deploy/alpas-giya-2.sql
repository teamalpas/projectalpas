-- MySQL dump 10.13  Distrib 5.6.22, for osx10.10 (x86_64)
--
-- Host: localhost    Database: alpas-giya
-- ------------------------------------------------------
-- Server version 5.6.22

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `activities`
--

DROP TABLE IF EXISTS `activities`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `activities` (
  `activity` varchar(255) DEFAULT NULL,
  UNIQUE KEY `activity` (`activity`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `activities`
--

LOCK TABLES `activities` WRITE;
/*!40000 ALTER TABLE `activities` DISABLE KEYS */;
INSERT INTO `activities` VALUES ('Islands-Beaches'), ('Mountains-Waterfalls'), ('Heritage-History'), ('Culture-Arts');
/*!40000 ALTER TABLE `activities` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `applicants`
--

DROP TABLE IF EXISTS `applicants`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `applicants` (
  `guide_id` varchar(255) NOT NULL,
  `guide_name` varchar(255) DEFAULT NULL,
  `contact_details` varchar(255) DEFAULT NULL,
  `address` varchar(255) DEFAULT NULL,
  `service` varchar(255) DEFAULT NULL,
  `status` varchar(255) DEFAULT NULL,
  `createdAt` datetime DEFAULT NULL,
  PRIMARY KEY `guide_id` (`guide_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `applicants`
--

LOCK TABLES `applicants` WRITE;
/*!40000 ALTER TABLE `applicants` DISABLE KEYS */;
/*!40000 ALTER TABLE `applicants` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `guides`
--

DROP TABLE IF EXISTS `guides`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `guides` (
  `location_id` varchar(255) DEFAULT NULL,
  `guide_id` varchar(255) DEFAULT NULL,
  `complete_name` varchar(255) DEFAULT NULL,
  `contact_details` varchar(255) DEFAULT NULL,
  `rate_details` varchar(255) DEFAULT "N/A",
  `service_description` varchar(255) DEFAULT NULL,
  `full_description` text DEFAULT NULL,
  `address` varchar(255) DEFAULT NULL,
  `facebook` varchar(255) DEFAULT NULL,
  `createdAt` datetime DEFAULT NULL,
  UNIQUE KEY `guide_id` (`guide_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `guides`
--

LOCK TABLES `guides` WRITE;
/*!40000 ALTER TABLE `guides` DISABLE KEYS */;
INSERT INTO `guides` VALUES 
  ('pangasinan-ib-1', 'pangasinan-ib-1-abcde00001', 'Ronald Secadron', '+639464540179', 350,
    'Services include tricycle ride around Pangasinan',
    'Services include tricycle ride around Pangasinan: Pickup from St. James the Great Parish to accomodation and drop off to bus terminal; Tour from Bolinao lighthouse, Rock View beach, Wonderful Cave, Bolinao Falls 1 & 2.',
    'Bolinao, Pangasinan', 'http://www.facebook.com/robbie.bautista', '2015-05-28 10:36:19'),
  ('pangasinan-ib-1', 'pangasinan-ib-1-abcde00002', 'Armando Lopez', '+639201440001', 600,
    'Services include tricycle ride around Pangasinan',
    'Services include tricycle ride around Pangasinan: Pickup from St. James the Great Parish to accomodation and drop off to bus terminal; Tour from Bolinao lighthouse, Rock View beach, Wonderful Cave, Bolinao Falls 1 & 2.',
    'Bolinao, Pangasinan', 'http://www.facebook.com/robbie.bautista', '2015-05-28 10:45:19'),
  ('pangasinan-ib-1', 'pangasinan-ib-1-abcde00003', 'Ferdinand Sungco', '+639201480019', 750,
    'Services include tricycle ride around Pangasinan',
    'Services include tricycle ride around Pangasinan: Pickup from St. James the Great Parish to accomodation and drop off to bus terminal; Tour from Bolinao lighthouse, Rock View beach, Wonderful Cave, Bolinao Falls 1 & 2.',
    'Bolinao, Pangasinan', 'http://www.facebook.com/robbie.bautista', '2015-05-28 10:50:19');
/*!40000 ALTER TABLE `guides` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `province_activity_map`
--

DROP TABLE IF EXISTS `province_activity_map`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `province_activity_map` (
  `province` varchar(255) DEFAULT NULL,
  `activity` varchar(255) DEFAULT NULL,
  `location_id` varchar(255) DEFAULT NULL,
  `location_details` varchar(255) DEFAULT NULL,
  `entry_count` int(11) DEFAULT NULL,
  `description` text DEFAULT NULL,
  `image_url` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `province_activity_map`
--

LOCK TABLES `province_activity_map` WRITE;
/*!40000 ALTER TABLE `province_activity_map` DISABLE KEYS */;
INSERT INTO `province_activity_map` VALUES 
  ('Cebu','Islands-Beaches', 'cebu-ib-1', 'Bantayan, Cebu', 3, 
    'Bantayan Island is a town located in Cebu, one of the 81 provinces of the Philippines. Climate is tropical, but there are two seasons: dry and wet. During the dry season, it is mostly sunny and humid. Tourism is low except during the summer months of March to July.', 
    'cebu-ib-1.jpg'),
  ('Cebu','Islands-Beaches', 'cebu-ib-2', 'Malapascua, Cebu', 3, 
    'Malapascua is a town located in Cebu, one of the 81 provinces of the Philippines. Climate is tropical, but there are two seasons: dry and wet. During the dry season, it is mostly sunny and humid. Tourism is low except during the summer months of March to July.', 
    'cebu-ib-2.jpg'),
  ('Cebu','Mountains-Waterfalls', 'cebu-mw-1', 'Osmeña Peak, Cebu', 3, 
    'Osmeña Peak is a mountain located in Cebu, one of the 81 provinces of the Philippines. Climate is tropical, but there are two seasons: dry and wet. During the dry season, it is mostly sunny and humid. Tourism is low except during the summer months of March to July.', 
    'cebu-mw-1.jpg'),
  ('ElNido','Islands-Beaches', 'elnido-ib-1', 'Nacpan Beach, El Nido', 3, 
    'Nacpan is a town located in El Nido, Palawan, one of the 81 provinces of the Philippines. Climate is tropical, but there are two seasons: dry and wet. During the dry season, it is mostly sunny and humid. Tourism is low except during the summer months of March to July.', 
    'elnido-ib-1.jpg'),
  ('ElNido','Islands-Beaches', 'elnido-ib-2', 'San Vicente, El Nido', 3, 
    'San Vicente is a town located in Palawan, one of the 81 provinces of the Philippines. Climate is tropical, but there are two seasons: dry and wet. During the dry season, it is mostly sunny and humid. Tourism is low except during the summer months of March to July.', 
    'elnido-ib-2.jpg'),
  ('ElNido','Islands-Beaches', 'elnido-ib-3', 'Corong Corong, El Nido', 3, 
    'Taraw Peak is a cliff located in El Nido, Palawan, one of the 81 provinces of the Philippines. Climate is tropical, but there are two seasons: dry and wet. During the dry season, it is mostly sunny and humid. Tourism is low except during the summer months of March to July.', 
    'elnido-ib-3.jpg'),
  ('ElNido','Mountains-Waterfalls', 'elnido-mw-1', 'Taraw Cliff, El Nido', 3, 
    'Corong Corong is a town located in El Nido, Palawan, one of the 81 provinces of the Philippines. Climate is tropical, but there are two seasons: dry and wet. During the dry season, it is mostly sunny and humid. Tourism is low except during the summer months of March to July.', 
    'elnido-mw-1.jpg'),
  ('Ifugao','Culture-Arts', 'ifugao-ca-1', 'Batad, Ifugao', 3, 
    'Batad is a town located in Ifugao, one of the 81 provinces of the Philippines. Climate is tropical, but there are two seasons: dry and wet. During the dry season, it is mostly sunny and humid. Tourism is low except during the summer months of March to July.', 
    'ifugao-ca-1.jpg'),
  ('Ifugao','Culture-Arts', 'ifugao-ca-2', 'Banaue, Ifugao', 3, 
    'Banaue is a town located in Ifugao, one of the 81 provinces of the Philippines. Climate is tropical, but there are two seasons: dry and wet. During the dry season, it is mostly sunny and humid. Tourism is low except during the summer months of March to July.', 
    'ifugao-ca-2.jpg'),
  ('Kalinga','Culture-Arts', 'kalinga-ca-1', 'Buscalan, Kalinga', 3, 
    'Buscalan is a town located in Kalinga, one of the 81 provinces of the Philippines. Climate is tropical, but there are two seasons: dry and wet. During the dry season, it is mostly sunny and humid. Tourism is low except during the summer months of March to July.', 
    'kalinga-ca-1.jpg'),
  ('Kalinga','Culture-Arts', 'kalinga-ca-2', 'Tinglayan, Kalinga', 3, 
    'Tinglayan is a town located in Kalinga, one of the 81 provinces of the Philippines. Climate is tropical, but there are two seasons: dry and wet. During the dry season, it is mostly sunny and humid. Tourism is low except during the summer months of March to July.', 
    'kalinga-ca-2.jpg'),
  ('Pangasinan','Islands-Beaches', 'pangasinan-ib-1', 'Bolinao, Pangasinan', 3, 
    'Bolinao is a town located in Pangasinan, one of the 81 provinces of the Philippines. Climate is tropical, but there are two seasons: dry and wet. During the dry season, it is mostly sunny and humid. Tourism is low except during the summer months of March to July.', 
    'pangasinan-ib-1.jpg'),
  ('Pangasinan','Islands-Beaches', 'pangasinan-ib-2', 'Alaminos, Pangasinan', 3, 
    'Alaminos is a town located in Pangasinan, one of the 81 provinces of the Philippines. Climate is tropical, but there are two seasons: dry and wet. During the dry season, it is mostly sunny and humid. Tourism is low except during the summer months of March to July.', 
    'pangasinan-ib-2.jpg'),
  ('Pangasinan','Islands-Beaches', 'pangasinan-ib-3', 'Rock View Beach, Pangasinan', 3, 
    'Rock View Beach is a beach located in Pangasinan, one of the 81 provinces of the Philippines. Climate is tropical, but there are two seasons: dry and wet. During the dry season, it is mostly sunny and humid. Tourism is low except during the summer months of March to July.', 
    'pangasinan-ib-3.jpg'),
  ('Pangasinan','Heritage-History', 'pangasinan-hh-1', 'Calasiao, Pangasinan', 3, 
    'Calasiao is a town located in Pangasinan, one of the 81 provinces of the Philippines. Climate is tropical, but there are two seasons: dry and wet. During the dry season, it is mostly sunny and humid. Tourism is low except during the summer months of March to July.', 
    'pangasinan-hh-1.jpg'),
  ('Rizal','Culture-Arts', 'rizal-ca-1', 'Angono, Rizal', 3, 
    'Angono is a town located in Rizal, one of the 81 provinces of the Philippines. Climate is tropical, but there are two seasons: dry and wet. During the dry season, it is mostly sunny and humid. Tourism is low except during the summer months of March to July.', 
    'rizal-ca-1.jpg'),
  ('Rizal','Culture-Arts', 'rizal-ca-2', 'Antipolo, Rizal', 3, 
    'Antipolo is a town located in Rizal, one of the 81 provinces of the Philippines. Climate is tropical, but there are two seasons: dry and wet. During the dry season, it is mostly sunny and humid. Tourism is low except during the summer months of March to July.', 
    'rizal-ca-2.jpg');
/*!40000 ALTER TABLE `province_activity_map` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `provinces`
--

DROP TABLE IF EXISTS `provinces`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `provinces` (
  `province` varchar(255) DEFAULT NULL,
  UNIQUE KEY `province` (`province`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `provinces`
--

LOCK TABLES `provinces` WRITE;
/*!40000 ALTER TABLE `provinces` DISABLE KEYS */;
INSERT INTO `provinces` VALUES ('Cebu'), ('ElNido'), ('Ifugao'), ('Kalinga'), ('Pangasinan'), ('Rizal');
/*!40000 ALTER TABLE `provinces` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2015-05-29 15:10:43
