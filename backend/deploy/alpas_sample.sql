-- MySQL dump 10.13  Distrib 5.6.22, for osx10.10 (x86_64)
--
-- Host: localhost    Database: alpas
-- ------------------------------------------------------
-- Server version 5.6.22

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `activities`
--

DROP TABLE IF EXISTS `activities`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `activities` (
  `activity` varchar(255) DEFAULT NULL,
  UNIQUE KEY `activity` (`activity`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `activities`
--

LOCK TABLES `activities` WRITE;
/*!40000 ALTER TABLE `activities` DISABLE KEYS */;
INSERT INTO `activities` VALUES ('beach'), ('services'), ('shopping');
/*!40000 ALTER TABLE `activities` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `applicants`
--

DROP TABLE IF EXISTS `applicants`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `applicants` (
  `guide_id` varchar(255) DEFAULT NULL,
  `complete_name` varchar(255) DEFAULT NULL,
  `contact_details` varchar(255) DEFAULT NULL,
  `rate_details` varchar(255) DEFAULT NULL,
  `service_description` varchar(255) DEFAULT NULL,
  `facebook` varchar(255) DEFAULT NULL,
  `address` varchar(255) DEFAULT NULL,
  `status` varchar(255) DEFAULT NULL,
  `createdAt` datetime DEFAULT NULL,
  UNIQUE KEY `guide_id` (`guide_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `applicants`
--

LOCK TABLES `applicants` WRITE;
/*!40000 ALTER TABLE `applicants` DISABLE KEYS */;
INSERT INTO `applicants` VALUES 
  ('laguna-shopping-4LC9B0oEgr','Giselle Palo','+639470000006','No details','No details',NULL,'Villa Olympia village San Pedro Laguna','pending','2015-05-28 11:34:16');
/*!40000 ALTER TABLE `applicants` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `guides`
--

DROP TABLE IF EXISTS `guides`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `guides` (
  `location_id` varchar(255) DEFAULT NULL,
  `guide_id` varchar(255) DEFAULT NULL,
  `complete_name` varchar(255) DEFAULT NULL,
  `contact_details` varchar(255) DEFAULT NULL,
  `rate_details` varchar(255) DEFAULT NULL,
  `service_description` varchar(255) DEFAULT NULL,
  `facebook` varchar(255) DEFAULT NULL,
  `address` varchar(255) DEFAULT NULL,
  `createdAt` datetime DEFAULT NULL,
  UNIQUE KEY `guide_id` (`guide_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `guides`
--

LOCK TABLES `guides` WRITE;
/*!40000 ALTER TABLE `guides` DISABLE KEYS */;
INSERT INTO `guides` VALUES 
  ('pangasinan-beach-1', 'pangasinan-beach-abcde00001', 'Ronald Secadron', '+639479876543', 'P700 per person', 'Services include boat during island hopping tour and lunch.', 'Pangasinan', NULL, '2015-05-28 10:36:19'),
  ('pangasinan-beach-1', 'pangasinan-beach-abcde00002', 'Emil Servito', '+639470000001', 'P300 per person', 'Services include tour only.', 'Pangasinan', NULL, '2015-05-28 10:41:05'),
  ('pangasinan-beach-1', 'pangasinan-beach-abcde00003', 'Lolita Cruz', '+639470000002', 'P500 per person', 'Services include tour and lunch.', 'Pangasinan', NULL, '2015-05-28 10:43:31'),
  ('pangasinan-beach-1', 'pangasinan-beach-abcde00004', 'Victorino Abad', '+639470000003', 'P450 per person', 'Services include tour only.', 'Pangasinan', NULL, '2015-05-28 11:12:28'),
  ('laguna-beach-1', 'laguna-beach-abcde00001', 'Ryan Busadre', '+639470000004', 'P1200 per person', 'Services include tour and picture taking.', 'San Pedro Laguna', NULL, '2015-05-28 11:20:59'),
  ('laguna-beach-2', 'laguna-beach-abcde00002', 'Angelika Marquez', '+639470000005', 'P450 per person', 'Services include tour and lunch.', '11th Street San Pedro Laguna', NULL, '2015-05-28 11:33:19');
/*!40000 ALTER TABLE `guides` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `province_activity_map`
--

DROP TABLE IF EXISTS `province_activity_map`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `province_activity_map` (
  `province` varchar(255) DEFAULT NULL,
  `activity` varchar(255) DEFAULT NULL,
  `location_id` varchar(255) DEFAULT NULL,
  `location_details` varchar(255) DEFAULT NULL,
  `entry_count` int(11) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `image_url` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `province_activity_map`
--

LOCK TABLES `province_activity_map` WRITE;
/*!40000 ALTER TABLE `province_activity_map` DISABLE KEYS */;
INSERT INTO `province_activity_map` VALUES 
  ('pangasinan','beach', 'pangasinan-beach-1', 'Patar Beach', 4, 'This is a description.', 'patar_beach.png'),
  ('pangasinan','beach', 'pangasinan-beach-2', 'Hundred Islands', 2, 'This is a description.', 'hundred_islands.png'),
  ('pangasinan','beach', 'pangasinan-beach-3', 'Bolinao Beach', 2, 'This is a description.', 'bolinao_beach.png'),
  ('pangasinan','shopping', 'pangasinan-shopping-1', 'Patar Seaside Mall', 2, 'This is a description.', 'patar_seaside_mall.png'),
  ('pangasinan','services', 'pangasinan-services-1', 'Patar Healing Spa', 1, 'This is a description.', 'patar_healing_spa.png'),
  ('laguna','beach', 'laguna-beach-1', 'Los Banos Hot Springs', 7, 'This is a description.', 'los_banos_hotsprings.png'),
  ('laguna','beach', 'laguna-beach-2', 'Laguna de Bay', 3, 'This is a description.', 'laguna_de_bay.png'),
  ('laguna','shopping', 'laguna-shopping-1', 'SM Sta. Rosa', 8, 'This is a description.', 'sm_sta_rosa.png'),
  ('cebu','shopping', 'cebu-shopping-1', 'Cebu Shopping Mall', 3, 'This is a description.', 'cebu_shopping_mall.png');
/*!40000 ALTER TABLE `province_activity_map` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `provinces`
--

DROP TABLE IF EXISTS `provinces`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `provinces` (
  `province` varchar(255) DEFAULT NULL,
  UNIQUE KEY `province` (`province`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `provinces`
--

LOCK TABLES `provinces` WRITE;
/*!40000 ALTER TABLE `provinces` DISABLE KEYS */;
INSERT INTO `provinces` VALUES ('cebu'), ('laguna'), ('pangasinan');
/*!40000 ALTER TABLE `provinces` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2015-05-29 15:10:43
