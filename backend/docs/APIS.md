# APIs

## `GET` /v1/activities

### Successful Response

HTTP Status Code: **200**
Return Values:

| Parameter  | Data Type |
| ---------- | --------- |
| activities | array		 |

Example:
```
{
  "success": true,
  "result": [
    "beach",
    "services",
    "shopping"
  ]
}
```

### Error Response

| Status Code | Local Code | Message          |
| ----------- | ---------- | ---------------- |
| 503					| -1				 | Database error   |
| 503					| -2 				 | No results found |

Example:
```
{
  "sucess" : false
  "code": -1,
  "message": "No results found"
}
```

## `GET` /v1/provinces

### Successful Response

HTTP Status Code: **200**
Return Values:

| Parameter  | Data Type |
| ---------- | --------- |
| provinces  | array		 |

Example:
```
{
  "success": true,
  "result": [
    "cebu",
    "laguna",
    "pangasinan"
  ]
}
```

### Error Response

| Status Code | Local Code | Message          |
| ----------- | ---------- | ---------------- |
| 503					| -1				 | Database error   |
| 503					| -2 				 | No results found |

Example:
```
{
  "success": false,
  "code": -1,
  "message": "No results found"
}
```

## `GET` /v1/search

### Significant Headers
**N/A**

### Query Parameters

  - page
  - province
  - activity

  **NOTE:** Query string can be a combination of these three. But it isn't a requirement that all or even one of them must be used. Not using any one of them would default to _"all"_ search.

  Example:
  ```
  /v1/search

  /v1/search?activity=beach&page=2
  ```

### Successful Response

HTTP Status Code: **200**
Return Values:

| Parameter  | Data Type |
| ---------- | --------- |
| provinces  | array		 |

Example:
```
{
  "success": true,
  "results": [
    {
      "province": "pangasinan",
      "activity": "beach",
      "location_id": "1:pangasinan-beach:1",
      "location_details": "Patar Beach",
      "entry_count": 4
    },
    {
      "province": "pangasinan",
      "activity": "beach",
      "location_id": "2:pangasinan-beach:2",
      "location_details": "Hundred Islands",
      "entry_count": 2
    }
  ],
  "current_page": 1,
  "total_pages": 5
}
```

### Error Response

| Status Code | Local Code | Message          |
| ----------- | ---------- | ---------------- |
| 503					| -1				 | Database error   |
| 503					| -2 				 | No results found |

Example:
```
{
  "success": true,
  "code": -1,
  "message": "No results found"
}
```

## `GET` /v1/location/:location_id

### Significant Headers
**N/A**

### Successful Response

HTTP Status Code: **200**
Return Values:

| Parameter  | Data Type |
| ---------- | --------- |
| results    | array     |

Example:
```
{
  "success": true,
  "results": [
    {
      "location_id": "pangasinan-beach-1",
      "guide_id": "pangasinan-beach-abcde00001",
      "complete_name": "Ronald Secadron",
      "contact_details": "+639479876543",
      "rate_details": "P700 per person",
      "service_description": "Services include boat during island hopping tour and lunch.",
      "facebook": "Pangasinan",
      "address": null,
      "createdAt": "2015-05-28T02:36:19.000Z"
    },
    {
      "location_id": "pangasinan-beach-1",
      "guide_id": "pangasinan-beach-abcde00002",
      "complete_name": "Emil Servito",
      "contact_details": "+639470000001",
      "rate_details": "P300 per person",
      "service_description": "Services include tour only.",
      "facebook": "Pangasinan",
      "address": null,
      "createdAt": "2015-05-28T02:41:05.000Z"
    }
  ],
  "current_page": 1,
  "total_pages": 2
}
```

### Error Response

| Status Code | Local Code | Message          |
| ----------- | ---------- | ---------------- |
| 503         | -1         | Database error   |
| 503         | -2         | No results found |

Example:
```
{
  "success": false
  "code": -1,
  "message": "No results found"
}
```

## `POST` /v1/applications

### Significant Headers
**N/A**

### Body Parameters

  - guide_name
  - contact_details
  - address
  - service

### Successful Response

HTTP Status Code: **200**
Return Values:

| Parameter  | Data Type |
| ---------- | --------- |
| guide_id   | string    |

Example:

**REQUEST:**
```
{
  "guide_name": "Alice Remigio",
  "contact_details": "+639470000007",
  "address": "11th Street Pacita Complex San Pedro Laguna",
  "service": "Shopping"
}
```

**RESPONSE:**

```
{
    "success": true,
    "guide_id": "Shopping-gpClqunzqN"
}
```

### Error Response

| Status Code | Local Code | Message                           |
| ----------- | ---------- | --------------------------------- |
| 503         | -1         | Database error                    |
| 400         | -3         | Paramete `param_name` is required |

Example:
```
{
    "success": false,
    "code": -1,
    "message": "Database error"
}
```

## `GET` /v1/applications

### Significant Headers
**N/A**

### Query Parameters

  - status (_OPTIONAL_)

  ```
  /v1/applications?status=pending
  /v1/applications?status=verified
  ```
  
### Successful Response

HTTP Status Code: **200**
Return Values:

| Parameter  | Data Type |
| ---------- | --------- |
| results    | array     |

Example:
```
{
    "success": true,
    "results": [
        {
            "guide_id": "Shopping-gpClqunzqN",
            "guide_name": "Alice Remigio",
            "contact_details": "+639470000007",
            "service": "Shopping",
            "address": "11th Street Pacita Complex San Pedro Laguna",
            "status": "pending",
            "createdAt": "2015-07-11T10:48:08.000Z"
        }
    ],
    "current_page": 1,
    "total_pages": 1
}
```

### Error Response

| Status Code | Local Code | Message                           |
| ----------- | ---------- | --------------------------------- |
| 503         | -1         | Database error                    |

Example:
```
{
  "success": false
  "code": -1,
  "message": "Database error"
}
```

## `PUT` /v1/applications/:guide_id

### Body Parameters

  - guide_name (_OPTIONAL_)
  - contact_details (_OPTIONAL_)
  - address (_OPTIONAL_)
  - service (_OPTIONAL_)

  **NOTE:** Combination of any of the four

### Successful Response

HTTP Status Code: **200**

Example:

**REQUEST:**
```
{
  "guide_name": "Alice Mae Remigio"
}
```

**RESPONSE:**

```
{
    "success": true
}
```

### Error Response

| Status Code | Local Code | Message                           |
| ----------- | ---------- | --------------------------------- |
| 503         | -1         | Database error                    |

Example:
```
{
    "success": false,
    "code": -1,
    "message": "Database error"
}
```

## `DELETE` /v1/applications/:guide_id

### Successful Response

HTTP Status Code: **200**

Example:

**RESPONSE:**

```
{
    "success": true
}
```

### Error Response

| Status Code | Local Code | Message                           |
| ----------- | ---------- | --------------------------------- |
| 503         | -1         | Database error                    |

Example:
```
{
    "success": false,
    "code": -1,
    "message": "Database error"
}
```

## `POST` /v1/guides

### Body Parameters

  - location_id
  - guide_id
  - complete_name 
  - contact_details
  - rate_details (_OPTIONAL_)
  - service_description
  - address
  - full_description (_OPTIONAL_)
  - address (_OPTIONAL_)
  - facebook (_OPTIONAL_)

  **NOTE:** Combination of any of the four

### Successful Response

HTTP Status Code: **200**

Example:

**REQUEST:**
```
{
  "location_id": "laguna-ca-1",
  "guide_id": "Shopping-ohVdZDjxCy",
  "complete_name": "Alice Remigio",
  "contact_details": "+639470000007",
  "service_description": "Sample service description",
  "address": "11th Street Pacita Complex San Pedro Laguna",
  "service": "Shopping"
}
```

**RESPONSE:**

```
{
    "success": true
}
```

### Error Response

| Status Code | Local Code | Message                           |
| ----------- | ---------- | --------------------------------- |
| 503         | -1         | Database error                    |

Example:
```
{
    "success": false,
    "code": -1,
    "message": "Database error"
}
```