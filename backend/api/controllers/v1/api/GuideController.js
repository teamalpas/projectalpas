module.exports = {
  list: function(req, res) {
    Guides.findOne({ guide_id : req.params.guide_id }, function(err, result) {
      if (err) {
        return res.json(503, {
          success: false,
          code: -1,
          message: 'Database error'
        });
      } 

      if (!result) {
        return res.json(503, {
          success: false,
          code: -2,
          message: 'No results found'
        });
      }

      res.json(200, {
        success: true,
        results: result
      });
    });
  },

  newGuide: function(req, res) {
    if (!req.body.location_id) {
      return res.json(400, {
        success : false,
        code    : -3,
        message : 'Parameter `location_id` is required.'
      });
    }

    if (!req.body.guide_id) {
      return res.json(400, {
        success : false,
        code    : -3,
        message : 'Parameter `guide_id` is required.'
      });
    }

    if (!req.body.complete_name) {
      return res.json(400, {
        success : false,
        code    : -3,
        message : 'Parameter `complete_name` is required.'
      });
    }

    if (!req.body.contact_details) {
      return res.json(400, {
        success : false,
        code    : -3,
        message : 'Parameter `contact_details` is required.'
      });
    }

    if (!req.body.service_description) {
      return res.json(400, {
        success : false,
        code    : -3,
        message : 'Parameter `service_description` is required.'
      });
    }

    if (!req.body.address) {
      return res.json(400, {
        success : false,
        code    : -3,
        message : 'Parameter `address` is required.'
      });
    }

    Guides.create(req.body, function(err, guide) {
      if (err) {
        return res.json(503, {
          success: false,
          code: -1,
          message: 'Database error'
        });
      }

      Applicants.update({ guide_id: req.body.guide_id }, { status: 'verified' }, function(err, applicant) {
        if (err) {
          return res.json(503, {
            success: false,
            code: -1,
            message: 'Database error'
          });
        }

        return res.json(200, { success: true });
      });
    });
  }
}