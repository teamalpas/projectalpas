var _ = require('underscore');
var async = require('async');
module.exports = {
  activities: function(req, res) {
    Activities.find({ sort: 'activity ASC'}, function(err, activities) {
      if (err) {
        // Handle errors here
        res.json(503, { success: false, code: -1, message: 'Database error' });
      }

      if (!activities) {
        res.json(503, { success: false, code: -2, message: 'No results found' });
      }

      var list = [];
      for (i in activities) {
        list.push(activities[i].activity);
      }
      res.json(200, { success: true, result: list });
    });
  },

  provinces: function(req, res) {
    Provinces.find({ sort: 'province ASC'}, function(err, provinces) {
      if (err) {
        // Handle errors here
        res.json(503, { success: false, code: -1, message: 'Database error' });
      }

      if (!provinces) {
        res.json(503, { success: false, code: -2, message: 'No results found' });
      }

      var list = [];
      for (i in provinces) {
        list.push(provinces[i].province);
      }
      res.json(200, { success: true, result: list });
    });
  },

  mapping: function(req, res) {
    var where = new Object();
    var skip;
    var limit = sails.config.page_max.province_activity;
    var sort = sails.config.sort_defaults.province_activity;
    var page = req.query.page;

    if (!page || page == 1) {
      skip = 0;
      page = 1;
    } else {
      skip = (req.query.page - 1) * limit;
    }

    if (req.query.province) {
      where['province'] = req.query.province;
      sort = 'activity ASC';
    }

    if (req.query.activity) {
      where['activity'] = req.query.activity;
      sort = 'province ASC'
    }

    var query = new Object();
    query['sort'] = sort;
    query['limit'] = limit;
    query['skip'] = skip;
    query['where'] = where;

    async.auto({
      countResults: function(callback) {
        ProvinceActivityMap.count(where, function(err, c) {
          if (err) {
            return callback({
              http_status: 503,
              code: -1,
              message: 'Database error'
            });
          }

          if (!c || c == 0) {
            return callback(null, { count: 0, query: { limit: sails.config.no_results_limit } });
          }

          callback(null, { total_pages: Math.ceil( c / limit )});
        });
      },

      getResults: ['countResults', function(callback, results) {
        var newQuery;
        if (results.countResults.count === 0) {
          newQuery = results.countResults.query;
        } else {
          newQuery = query;
        }

        ProvinceActivityMap.find(newQuery, function(err, results) {
          if (err) {
            return callback({
              http_status: 503,
              code: -1,
              message: 'Database error'
            });
          }

          if (!results) {
            return callback({
              http_status: 503,
              code: -2,
              message: 'No results found'
            });
          }

          callback(null, { values: results });
        });
      }]
    }, function(error, result) {
      if (error) {
        res.json(error.http_status, {
          success: false,
          code: error.code,
          message: error.message
        });
      } else {
        res.json(200, { 
          success: true,
          results: result.getResults.values,
          current_page: parseInt(page, 10),
          total_pages: result.countResults.total_pages,
          image_host: sails.config.image_host,
        });
      }
    });
  },

  locations: function(req, res) {
    var query = new Object();
    var location_id 
    if (sails.config.environment === 'development') {
      location_id = sails.config.test_location_id;
    } else {
      location_id = req.params.location_id;
    }
    
    query['where'] = { location_id: location_id };
    query['sort'] = sails.config.sort_defaults.location_guide;

    var page = req.query.page || 1;
    var limit = sails.config.page_max.location_guide;
    query['skip'] = (!page || page == 1) ? 0 : (req.query.page - 1) * limit;
    query['limit'] = limit;

    async.auto({
      countResults: function(callback) {
        Guides.count(query['where'], function(err, c) {
          if (err) {
            return callback({
              http_status: 503,
              code: -1,
              message: 'Database error'
            });
          }

          if (!c) {
            return callback({
              http_status: 503,
              code: -2,
              message: 'No results found'
            });
          }

          callback(null, { total_pages: Math.ceil( c / query['limit'] ) });
        });
      },

      getResults: function(callback) {
        Guides.find(query, function(err, results) {
          if (err) {
            return callback({
              http_status: 503,
              code: -1,
              message: 'Database error'
            });
          }

          if (!results) {
            return callback({
              http_status: 503,
              code: -2,
              message: 'No results found'
            });
          }

          callback(null, { values: results });
        });  
      },

      mappingResults: function(callback) {
        ProvinceActivityMap.findOne({ location_id: req.params.location_id }, function(err, result) {
          if (err) {
            return callback({
              http_status: 503,
              code: -1,
              message: 'Database error'
            });
          }

          if (!result) {
            return callback({
              http_status: 503,
              code: -2,
              message: 'No results found'
            });
          }

          callback(null, { description: result.description, image_url: sails.config.image_host + result.image_url });
        });
      }
    }, function(error, result) {
      if (error) {
        res.json(error.http_status, {
          success: false,
          code: error.code,
          message: error.message
        });
      } else {
        res.json(200, { 
          success: true,
          description: result.mappingResults.description,
          image_url: result.mappingResults.image_url,
          results: result.getResults.values,
          current_page: parseInt(page, 10),
          total_pages: result.countResults.total_pages
        });
      }
    });
  }
  
}