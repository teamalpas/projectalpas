function generate_salt() {
  var chars = '1234567890abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
  var length = sails.config.guide_details.id_length;
  var string = '';
  for (var i = 0; i < length; i++) {
      var randomNumber = Math.floor(Math.random() * chars.length);
      string += chars.substring(randomNumber, randomNumber + 1);
  }
  return string;
}

module.exports = {
  initialApplication: function(req, res) {
    if (!req.body.guide_name) {
      return res.json(400, {
        success : false,
        code    : -3,
        message : 'Parameter `guide_name` is required.'
      });
    }

    if (!req.body.contact_details) {
      return res.json(400, {
        success : false,
        code    : -3,
        message : 'Parameter `contact info` is required.'
      });
    }

    if (!req.body.address) {
      return res.json(400, {
        success : false,
        code    : -3,
        message : 'Parameter `address` is required.'
      });
    }

    if (!req.body.service) {
      return res.json(400, {
        success : false,
        code    : -3,
        message : 'Parameter `service` is required.'
      });
    }

    var guide_id = req.body.service + '-' + generate_salt();
    Applicants.create({
      guide_id        : guide_id,
      guide_name      : req.body.guide_name,
      contact_details : req.body.contact_details,
      address         : req.body.address,
      service         : req.body.service
    }, function(err) {
      if (err) {
        return res.json(503, {
          success : false,
          code    : -1,
          message : 'Database error'
        });
      }

      res.json(200, {
        success: true,
        guide_id: guide_id
      });
    });
  },

  existingApplications: function(req, res) {
    var query = new Object();
    var page = req.query.page || 1;
    var limit = sails.config.page_max.application_list;
    query['skip'] = (!page || page == 1) ? 0 : (req.query.page - 1) * limit;
    query['limit'] = limit;

    if (req.query.status) {
      query['where'] = { status: req.query.status };
    }

    async.auto({
      countResults: function(callback) {
        Applicants.count(function(err, c) {
          if (err) {
            return callback({
              http_status: 503,
              code: -1,
              message: 'Database error'
            });
          }

          if (!c) {
            return callback(null, {
              total_pages: 1
            });
          }

          callback(null, { total_pages: Math.ceil( c / query['limit'] ) });
        });
      },

      getResults: function(callback) {
        Applicants.find(query, function(err, applicants) {
          if (err) {
            return callback({
              http_status: 503,
              code: -1,
              message: 'Database error'
            });
          }

          if (!applicants) {
            return callback({
              http_status: 503,
              code: -2,
              message: 'No results found'
            });
          }

          callback(null, { values: applicants });
        });
      }

    }, function(error, result) {
      if (error) {
        res.json(error.http_status, {
          success : false,
          code    : error.code,
          message : error.message
        });
      } else {
        res.json(200, { 
          success       : true,
          results       : result.getResults.values,
          current_page  : parseInt(page, 10),
          total_pages   : result.countResults.total_pages
        });
      }
    });
  },

  editApplication: function(req, res) {
    var keyvalue = {};
    var obj = _.pairs(req.body);
    
    _.each(obj, function(pair){
      keyvalue[pair[0]] = pair[1];
    });
    
    console.log(req.params.guide_id);
    console.log(keyvalue);
    Applicants.update({ 
      guide_id: req.params.guide_id
    }, keyvalue, function(err, packages){
      if (err) {
        return res.json(503, {
          success : false,
          code    : -1,
          message : 'Database error'
        });
      }
  
      return res.json(200, { success: true });
    });
  },

  deleteApplication: function(req, res) {
    Applicants.destroy({ guide_id: req.params.guide_id }, function(err){
      if (err) {
        res.json(503, {
          success : false,
          code    : -1,
          message : 'Database error'
        });
      }
      
      res.json(200, { success: true });
    });
  }
}