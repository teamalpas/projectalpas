/*
  List of Provinces
 */

module.exports = {
  connection: 'mysql',
  schema: true, 
  tableName: 'provinces',
  autoCreatedAt: false,
  autoUpdatedAt: false,
  autoPK: false,
  attributes: {
    province: {
      type: 'string',
      unique: true
    }
  }
}