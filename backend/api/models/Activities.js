/*
  List of Activities
 */

module.exports = {
  connection: 'mysql',
  schema: true, 
  tableName: 'activities',
  autoCreatedAt: false,
  autoUpdatedAt: false,
  autoPK: false,
  attributes: {
    activity: {
      type: 'string',
      unique: true
    }
  }
}