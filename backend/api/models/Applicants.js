/*
  Location-Guide Mapping
 */

module.exports = {
  connection: 'mysql',
  schema: true, 
  tableName: 'applicants',
  autoCreatedAt: true,
  autoUpdatedAt: false,
  autoPK: false,
  attributes: {
    guide_id: {
      type: 'string',
      primaryKey: true
    },
    guide_name: {
      type: 'string'
    },
    contact_details: {
      type: 'string',
    },
    service: {
      type: 'string'
    },
    address: {
      type: 'string',
      defaultsTo: null
    },
    status: {
      type: 'string',
      enum: ['pending', 'verified'],
      defaultsTo: 'pending'
    }
  }
}