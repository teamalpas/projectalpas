/*
  Location-Guide Mapping
 */

module.exports = {
  connection: 'mysql',
  schema: true, 
  tableName: 'guides',
  autoCreatedAt: true,
  autoUpdatedAt: false,
  autoPK: false,
  attributes: {
    location_id: {
      type: 'string',
      defaultsTo: null
    },
    guide_id: {
      type: 'string',
      unique: 'true'
    },
    complete_name: {
      type: 'string'
    },
    contact_details: {
      type: 'string',
    },
    rate_details: {
      type: 'string',
      defaultsTo: 'No details'
    },
    service_description: {
      type: 'string',
      defaultsTo: 'No details'
    },
    full_description: {
      type: 'string',
      defaultsTo: 'No details'
    },
    facebook: {
      type: 'string',
      defaultsTo: null
    },
    address: {
      type: 'string',
      defaultsTo: null
    }
  }
}