/*
  Province-Activity Mapping
 */

module.exports = {
  connection: 'mysql',
  schema: true, 
  tableName: 'province_activity_map',
  autoCreatedAt: false,
  autoUpdatedAt: false,
  autoPK: false,
  attributes: {
    province: {
      type: 'string'
    },
    activity: {
      type: 'string'
    },
    location_id: {
      type: 'string'
    },
    location_details: {
      type: 'string'
    },
    entry_count: {
      type: 'integer',
      defaultsTo: 0
    },
    description: {
      type: 'string'
    },
    image_url: {
      type: 'string'
    }
  }
}