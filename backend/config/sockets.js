/**
 * WebSocket Server Settings
 * (sails.config.sockets)
 */

module.exports.sockets = {

  /***************************************************************************
  *                                                                          *
  * This custom onConnect function will be run each time AFTER a new socket  *
  * connects (To control whether a socket is allowed to connect, check out   *
  * `authorization` config.) Keep in mind that Sails' RESTful simulation for *
  * sockets mixes in socket.io events for your routes and blueprints         *
  * automatically.                                                           *
  *                                                                          *
  ***************************************************************************/
  onConnect: function(session, socket) {

    // By default, do nothing.

  },


  /***************************************************************************
  *                                                                          *
  * This custom onDisconnect function will be run each time a socket         *
  * disconnects                                                              *
  *                                                                          *
  ***************************************************************************/
  onDisconnect: function(session, socket) {

    // By default: do nothing.
  },
};
