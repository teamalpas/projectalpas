/**
 * Route Mappings
 * (sails.config.routes)
 */

module.exports.routes = {
  // For querying of available activities/provinces (so that front end need not have to change from time to time)
  'get /v1/activities'  : 'v1/api/QueryController.activities',
  'get /v1/provinces'   : 'v1/api/QueryController.provinces',

  // Get the list of activities/provinces given the selected province/activity
  'get /v1/search'    : 'v1/api/QueryController.mapping',

  // Get list of guides given the location_id
  'get /v1/locations/:location_id'  : 'v1/api/QueryController.locations',

  // Get guide info based from guide_id
  'get /v1/guides/:guide_id'  : 'v1/api/GuideController.list',
  'post /v1/guides'           : 'v1/api/GuideController.newGuide',

  // Manage local application
  'get /v1/applications'             : 'v1/api/ApplicationController.existingApplications',
  'post /v1/applications'             : 'v1/api/ApplicationController.initialApplication',
  'put /v1/applications/:guide_id'    : 'v1/api/ApplicationController.editApplication',
  'delete /v1/applications/:guide_id' : 'v1/api/ApplicationController.deleteApplication'
}
