/**
 * View Engine Configuration
 * (sails.config.views)
 */

module.exports.views = {
  engine: 'ejs',
  layout: 'layout'
};
