# GIYA WEB APPLICATION
Giya Web Application is currently running in AWS t2.micro EC2 instance. The server uses Amazon Linux for its OS.

## Project Description

## Technical Details

### Deployment Guide

#### Frontend

##### Dependencies

- php55
- php55-fpm
- nginx

##### Installation Guide

- Edit the lines concerned below in **/etc/php-fpm-5.5.d/www.conf**

```
listen = /var/run/php5-fpm.sock

listen.owner = ec2-user
listen.group = ec2-user
listen.mode = 0660

user = ec2-user
group = ec2-user
```

**NOTE:** You may edit other lines as you wish. The ones noted are the basic ones to make the app running.

- Start (or restart) php-fpm-5.5

```
$ sudo /etc/init.d/php-fpm-5.5 start
or
$ sudo /etc/init.d/php-fpm-5.5 restart
```

- Copy **virtual.conf** in nginx folder to /etc/nginx/conf.d/virtual.conf

```
$ sudo cp virtual.conf /etc/nginx/conf.d/virtual.conf
```

- Start/Restart nginx

```
$ sudo service nginx start
or
$ sudo service nginx restart
```

#### Backend

**NOTE:** For smooth replication, run the following in a Linux/Unix server

- Install **Node.js** (use v0.10.*), **git** and **mysql**

Linux (Ubuntu): 
```
$ sudo yum install node
$ sudo yum install git
$ sudo yum install mysql
```

Mac OSx:
```
$ brew install node
$ brew install git
$ brew install mysql
```

- Clone Team Alpas repo

```
$ git clone https://eapesa@bitbucket.org/teamalpas/projectalpas.git
```

- Navigate to **backend** folder

```
$ cd projectalpas/backend
```

- Install web server dependencies

```
$ npm install sails-disk
$ npm install
```

- To be continued...

### Serving Static Files

Static files are stored in **/images/FILENAME.EXTENSION**. To access these files, use the following URL:

```
HOST:PORT/images/FILENAME.EXTENSION
```